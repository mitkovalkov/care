<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;
use App\Http\Controllers\FrontController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->group(function () {
    Route::group(['middleware' => ['auth', 'role:moderator'], 'as' => 'admin.'], function () {

        Route::redirect('/', '/admin/dashboard', 301);
        Route::get('dashboard', [Admin\AdminController::class, 'index'])->name('index');
        Route::get('logs', [Admin\AdminController::class, 'logs'])->name('logs.index');
        
        Route::get('translations', function() { return view('admin.translations-index'); })->name('translations.index');
        
        Route::resource('langs', Admin\LanguageController::class);
        
        Route::get('pages/{page}/fill', [Admin\PageController::class, 'fill'])->name('pages.fill');
        Route::post('pages/{page}/fill', [Admin\PageController::class, 'fillSave'])->name('pages.fill.save');
        Route::resource('pages', Admin\PageController::class);
        
        Route::resource('testimonials', Admin\TestimonialController::class);

        Route::resource('counters', Admin\CounterController::class);
        Route::resource('timelines', Admin\TimelineController::class);

        Route::resource('jobs', Admin\JobController::class);

    });
});
$lang = 'bg';
Route::redirect('/',$lang);
Route::redirect('/login', "$lang/login");
Route::redirect('/register', "$lang/register");
Route::prefix('{language?}')->group(function () {
    Route::get('/', [FrontController::class, 'indexPage'])->name('index');

    Route::get('/patient-support-program', [FrontController::class, 'pspPage'])->name('inner-page');
    Route::get('/homecare', [FrontController::class, 'homeCare'])->name('homecare');
    Route::get('/covid-19', [FrontController::class, 'covidPage'])->name('covid-19');
    Route::get('/post-covid', [FrontController::class, 'postCovidPage'])->name('post-covid');
    Route::get('/cardiology', [FrontController::class, 'cardiologyPage'])->name('cardiology');

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/contact', function () {
        return view('pages.contact');
    })->name('contact');

    Route::get('/company', [FrontController::class, 'companyPage'])->name('company');

    Auth::routes();
    
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    
    Route::get('/jobs', [FrontController::class, 'jobsIndex'])->name('jobs.index');
    Route::get('/jobs/{job}', [FrontController::class, 'jobsShow'])->name('jobs.show');
    
    Route::get('/{slug}', [FrontController::class, 'show'])->name('page.show');
    
});