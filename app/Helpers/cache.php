<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Кешира eloquent заявка
 *
 * @param Builder $eloquent
 * @param string $finish по подразбиране е get. За странициране - ['paginate', $page, $per_page]
 * @param integer|null $seconds При null се запомня за постоянно
 * @return null|object връща резултата от заявката 
 */
function cacheQuery(Builder $eloquent, $finish = 'get', $seconds = null)
{
    $sql = sqlDump($eloquent);
    $per_page = null;
    $page = null;
    if (is_array($finish)) {
        $page = $finish[1];
        $per_page = (isset($finish[2])) ? $finish[2] : paginate;
        $finish = $finish[0];
        $sql .= "paginate $page";
    }

    // ако е в режим на разработване - да не се кешира
    // TODO да се провери настройката в .env файла
    if(config('app.debug')) {
        if ($finish == 'paginate') {
            return $eloquent->paginate($per_page, ['*'], 'page', $page);
        }
        return $eloquent->$finish();
    }

    return Cache::remember(md5($sql), $seconds, function () use ($sql, $eloquent, $finish, $page, $per_page) {
        if ($finish == 'paginate') {
            return $eloquent->paginate($per_page, ['*'], 'page', $page);
        }
        return $eloquent->$finish();
    });
}
/**
 *
 * @param Builder $eloquent
 * @return string sql
 */
function sqlDump(Builder $eloquent)
{
    $sql = $eloquent->toSql();
    $bindings = $eloquent->getBindings();
    foreach ($bindings as $replace) {
        $pos = strpos($sql, '?');
        if ($pos !== false) {
            $sql = substr_replace($sql, "'$replace'", $pos, 1);
        }
    }
    return $sql;
}
/**
 * Изтрива заявка от кеша
 *
 * @param string $key
 * @return bool
 */
function cacheForget(Builder $eloquent)
{
    $key = sqlDump($eloquent);
    return Cache::forget(md5($key));
}
