<?php

use App\Models\Log;
use App\Repositories\LanguageRepository;
use App\Repositories\PageRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

define('paginate', 9); // по колко на брой елемента да има при странициране

/**
 * За по-удобно взимане на данните
 * @param string $slug
 * @return array
 */
function page($slug)
{
    $params = [];
    $page = PageRepository::findBySlug($slug);
    $params['page'] = $page;
    $params['data'] = $page->data;  
    return $params;
}

/**
 * хелпер, с който се връща route с текущият език
 *
 * @param string $path
 * @param array $params
 * @param string $lang с кой език да бъде сменен
 * @return void
 */
function r($path, $params = [], $lang = null) {
    $locale = $lang ?? app()->getLocale();
    array_unshift($params, $locale);
    return route($path, $params);
}

/**
 * Смяна към друг език.
 * @param string $lang
 * @param string $route
 * @return string
 */
function changeLang($lang, $route = null) {
    if ($route) {
        $crp = [];
        $crp[0] = $lang;
        return route($route, $crp);
    }
    if (request()->route() == null) {
        return route('index',[$lang]);
    }
    $crp = \Route::getCurrentRoute()->parameterNames;
    $crp[0] = $lang;
    $crp_count = count($crp);

    for ($i = 1; $i < $crp_count; $i++) {
        $crp[$i] = request($crp[$i]);
    }
    return route(\Route::getCurrentRoute()->action['as'], $crp);
}

/**
 * Логване на активност
 * @param $model_type
 * @param $method
 * @param null $model_id
 * @param null $user_id
 * @param null $message
 * @return bool|mixed|null
 */
function activity_log($model_type, $method, $model_id = null, $user_id = null, $message = null)
{
    try {
        $log = new Log();
        $log->model_type = $model_type;
        $log->method = $method;
        $log->model_id = $model_id;
        $log->user_id = $user_id;
        $log->message = $message;
        $log->save();
        return $log->id;
    } catch (\Exception $e) {
        return false;
    }
    return null;
}

/**
 * Ако текста е по-дълъг от подаденият размер,
 * го изрязва до последната дума и добавя "..."
 *
 * @param string $text
 * @param integer $size
 * @return string
 */
function shortText($text, $size)
{
    if (strlen($text) <= $size) {
        return $text;
    }
    $s = substr($text, 0, $size);
    return substr($s, 0, strrpos($s, ' ')) . '...';
}

/**
 * Форматира дата по подаден формат.
 *
 * @param string $value Примерно created_at
 * @param string $format
 * @return void
 */
function format_datetime($value, $format = 'd.m.Y')
{
    return \Carbon\Carbon::parse($value)->format($format);
}

/**
 * Дали формата е на снимка
 *
 * @param string $format
 * @return boolean
 */
function is_format_image($format) {
    // стандартни формати
    if(in_array(strtolower($format), ['jpeg','jpg','png','gif', 'svg'])) {
        return true;
    }
    return false;
}

/**
 * Дали пътя е снимка
 *
 * @param string $filepath
 * @return boolean
 */
function is_image($filepath) {
    $dot = strrpos($filepath, '.') + 1;
    $format = substr($filepath, $dot, strlen($filepath) - $dot);
    return is_format_image($format);
}

/**
 * Името на езика спрямо ID-то
 * @param $lang_id
 * @return string
 */
function langName($lang_id) {
    $lang = LanguageRepository::find($lang_id);
    return $lang->locale;
}

/**
 * ID-то спрямо името на езика
 * @param $lang_name
 * @return int
 */
function langId($locale) {
    $lang = LanguageRepository::findByLocale($locale);
    return $lang->id;
}

function getLang() {
    $lang = LanguageRepository::findByLocale(App::getLocale());
    return $lang;
}

/**
 * ID-то на текущият език
 * @return int
 */
function currentLangId() {
    return langId(App::getLocale());
}

function langs() {
    static $langs = null;
    if($langs === null) {
        $langs = LanguageRepository::all();
    }
    return $langs;
}
