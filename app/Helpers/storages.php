<?php

use Illuminate\Http\UploadedFile;

/**
 * Помощна функция за записване на файл в локалният драйвер
 *
 * Пример за създаден локален драйвер (създава се в config/filesystems)
 * 'img' => [
'driver' => 'local',
'root' => public_path('img'),
'url' => env('APP_URL').'/img',
],
 * При проблеми да се обърне внимание дали настройките са кеширани php artisan config:clear
 *
 * @see config/filesystems.php
 * @param UploadedFile $file примерно $input['img']
 * @param string $disk
 * @param null|string $name с какво име да бъде записан файла
 * @return string пътя до файла
 */
function storeFileLocal(UploadedFile $file, $disk, $name = null)
{
    if (!$name) {
        $name = $file->getClientOriginalName();
    } else {
        $explodeName = explode('.', $name);
        if (count($explodeName) < 2) { // няма разширение
            $name .= '.' . $file->getClientOriginalExtension();
        }
    }
    return $file->storeAs('', $name, $disk);
}

/**
 * Помощна фукнция за взимане на URL адрес на файл от диск
 * Всеки диск трябва да има url подаден
 *
 * @param string $file_path Адреса на снимката. Приемерно Avatar.png
 * @param string $disk
 * @return string
 */
function getFileUrl($file_path, $disk)
{

    $url = config("filesystems.disks.$disk.url");
    if (!$url) {
        return asset($file_path);
    }
    return $url . "/$file_path";
}
/**
 * Записване на файл 
 *
 * @param UploadedFile $file
 * @param null|string $name с какво име да бъде записан файла
 * @return string
 */
function storeImage($file, $name = null)
{
    return storeFileLocal($file, 'img', $name);
}
function getImageUrl($file_path)
{
    return getFileUrl($file_path, 'img');
}

function storeFlag($file, $name = null)
{
    return storeFileLocal($file, 'flags', $name);
}
function getFlagUrl($file_path)
{
    return getFileUrl($file_path, 'flags');
}

function storePage($file, $name = null)
{
    return storeFileLocal($file, 'pages', $name);
}
function getPageUrl($file_path)
{
    return getFileUrl($file_path, 'pages');
}

function storeTestimonial($file, $name = null)
{
    return storeFileLocal($file, 'testimonials', $name);
}
function getTestimonialUrl($file_path)
{
    return getFileUrl($file_path, 'testimonials');
}

function storeCounter($file, $name = null)
{
    return storeFileLocal($file, 'counter', $name);
}
function getCounterUrl($file_path)
{
    return getFileUrl($file_path, 'counter');
}