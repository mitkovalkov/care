<?php

namespace App\Repositories;

use App\Models\Counter;
use App\Models\Translate;

class CounterRepository
{
    public static function find($id)
    {
        return cacheQuery(Counter::where('id', $id), 'firstOrFail');
    }

    public static function all()
    {
        return Counter::orderBy('id', 'asc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Counter::orderBy('id', 'asc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $counter = new Counter;

        return self::update($counter, $input);
    }

    public static function update($counter, array $input)
    {
        $counter->color = $input['color'];
        $counter->save();

        if (isset($input['img'])) {
            $counter->img = storeCounter($input['img'], $counter->id);
            $counter->save();
        }

        foreach ($input['data'] as $lang_id => $elements) {

            $lang = Translate::updateOrCreate(
                ['object_type' => 'App\Models\Counter', 'object_id' => $counter->id, 'language_id' => $lang_id],
                ['value' => ($elements)]
            );
        }
        cache()->flush();
        return $counter;
    }

    public static function delete($counter)
    {
        $counter->delete();
        cache()->flush();
    }
}
