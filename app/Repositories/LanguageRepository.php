<?php

namespace App\Repositories;

use App\Models\Language;

class LanguageRepository
{
    public static function find($id)
    {
        return cacheQuery(Language::where('id', $id), 'firstOrFail');
    }
    
    public static function findByLocale($locale)
    {
        return cacheQuery(Language::where('locale', $locale), 'firstOrFail');
    }
    
    public static function all()
    {
        return Language::orderBy('name', 'asc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Language::orderBy('name', 'asc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $Language = new Language;

        return self::update($Language, $input);
    }

    public static function update($Language, array $input)
    {
        $Language->name = $input['name'];
        $Language->save();

        if (isset($input['flag'])) {
            $Language->flag = storeFlag($input['flag'], $Language->code);
            $Language->save();
        }

        cache()->flush();
        return $Language;
    }

    public static function delete($Language)
    {
        $Language->delete();
        cache()->flush();
    }
}
