<?php

namespace App\Repositories;

use App\Models\Job;
use App\Models\Translate;

class JobRepository
{
    public static function find($id)
    {
        return cacheQuery(Job::where('id', $id), 'firstOrFail');
    }

    public static function all()
    {
        return Job::orderBy('id', 'desc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Job::orderBy('id', 'desc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $job = new Job;

        return self::update($job, $input);
    }

    public static function update($job, array $input)
    {
        $job->link = $input['link'];
        $job->save();

        foreach ($input['data'] as $lang_id => $elements) {

            $lang = Translate::updateOrCreate(
                ['object_type' => 'App\Models\Job', 'object_id' => $job->id, 'language_id' => $lang_id],
                ['value' => ($elements)]
            );
        }
        cache()->flush();
        return $job;
    }

    public static function delete($job)
    {
        $job->delete();
        cache()->flush();
    }
}
