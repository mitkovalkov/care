<?php

namespace App\Repositories;

use App\Models\Timeline;
use App\Models\Translate;

class TimelineRepository
{
    public static function find($id)
    {
        return cacheQuery(Timeline::where('id', $id), 'firstOrFail');
    }

    public static function all()
    {
        return Timeline::orderBy('id', 'desc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Timeline::orderBy('id', 'desc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $timeline = new Timeline;

        return self::update($timeline, $input);
    }

    public static function update($timeline, array $input)
    {
        $timeline->name = $input['name'];
        $timeline->color = $input['color'];
        $timeline->save();

        foreach ($input['data'] as $lang_id => $elements) {

            $lang = Translate::updateOrCreate(
                ['object_type' => 'App\Models\Timeline', 'object_id' => $timeline->id, 'language_id' => $lang_id],
                ['value' => ($elements)]
            );
        }
        cache()->flush();
        return $timeline;
    }

    public static function delete($timeline)
    {
        $timeline->delete();
        cache()->flush();
    }
}
