<?php

namespace App\Repositories;

use App\Models\Page;
use App\Models\Translate;

class PageRepository
{
    public static function find($id)
    {
        return cacheQuery(Page::where('id', $id), 'firstOrFail');
    }
    
    public static function findBySlug($slug)
    {
        return cacheQuery(Page::where('slug', $slug), 'firstOrFail');
    }

    public static function all()
    {
        return Page::orderBy('created_at', 'desc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Page::orderBy('created_at', 'desc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $page = new Page;

        return self::update($page, $input);
    }

    public static function update($page, array $input)
    {
        $page->name = $input['name'];
        $page->slug = $input['slug'];
        $page->settings = $input['settings'];
        $page->save();
        cache()->flush();
        return $page;
    }

    public static function delete($page)
    {
        $page->delete();
        cache()->flush();
    }

    public static function fill($page, $input)
    {
        foreach ($input['data'] as $lang_id => $elements) {

            $lang = Translate::updateOrCreate(
                ['object_type' => 'App\Models\Page', 'object_id' => $page->id, 'language_id' => $lang_id ],
                ['value' => ($elements)]
            );
        }
        cache()->flush();
        return $page;
    }
}
