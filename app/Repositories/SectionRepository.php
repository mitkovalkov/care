<?php

namespace App\Repositories;

use App\Models\Template;
use App\Models\Section;
use Illuminate\Support\Facades\DB;

class SectionRepository
{
    public static function find($id)
    {
        return cacheQuery(Section::where('id', $id), 'firstOrFail');
    }

    public static function all($template_id = null)
    {
        return Section::orderBy('name', 'asc')
        ->when($template_id, function($query) use($template_id) {
            $query->where('template_id', $template_id);
        })->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Section::orderBy('name', 'asc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $section = new Section;

        return self::update($section, $input);
    }

    public static function update($section, array $input)
    {
        $section->template_id = $input['template_id'];
        $section->name = $input['name'];
        $section->save();
        cache()->flush();
        return $section;
    }
    
    public static function delete($section)
    {
        $section->delete();
        cache()->flush();
    }
}
