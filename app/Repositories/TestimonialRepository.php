<?php

namespace App\Repositories;

use App\Models\Testimonial;
use App\Models\Translate;

class TestimonialRepository
{
    public static function find($id)
    {
        return cacheQuery(Testimonial::where('id', $id), 'firstOrFail');
    }

    public static function all()
    {
        return Testimonial::orderBy('id', 'desc')->get();
    }

    public static function paginate($page)
    {
        return cacheQuery(Testimonial::orderBy('id', 'desc'), ['paginate', $page]);
    }

    public static function create(array $input)
    {
        $testimonial = new Testimonial;

        return self::update($testimonial, $input);
    }

    public static function update($testimonial, array $input)
    {
        $testimonial->save();

        if (isset($input['img'])) {
            $testimonial->img = storeTestimonial($input['img'], $testimonial->id);
            $testimonial->save();
        }

        foreach ($input['data'] as $lang_id => $elements) {

            $lang = Translate::updateOrCreate(
                ['object_type' => 'App\Models\Testimonial', 'object_id' => $testimonial->id, 'language_id' => $lang_id],
                ['value' => ($elements)]
            );
        }
        cache()->flush();
        return $testimonial;
    }

    public static function delete($testimonial)
    {
        $testimonial->delete();
        cache()->flush();
    }
}
