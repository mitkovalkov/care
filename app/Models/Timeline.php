<?php

namespace App\Models;

use App\Models\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use HasFactory, TranslateTrait;

    protected $fillable = ['name', 'color'];


    public function content($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('content', $lang->id);
    }
}
