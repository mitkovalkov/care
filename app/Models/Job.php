<?php

namespace App\Models;

use App\Models\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory, TranslateTrait;

    protected $fillable = ['link'];

    public function name($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('name', $lang->id);
    }

    public function content($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('content', $lang->id);
    }
}
