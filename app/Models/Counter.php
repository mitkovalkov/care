<?php

namespace App\Models;

use App\Models\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    use HasFactory, TranslateTrait;

    protected $fillable = ['img', 'color'];

    public function img() {
        if($this->img) {
            return getCounterUrl($this->img);
        }
        return null;
    }
    
    public function name($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('name', $lang->id);
    }
    
    public function val($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('val', $lang->id);
    }

    public function content($lang = null)
    {
        if (!$lang) {
            $lang = getLang();
        }
        return $this->t('content', $lang->id);
    }
}
