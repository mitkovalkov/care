<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_id',
        'object_type',
        'object_id',
        'value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    
    public function object() {
        return $this->morphTo();
    }

    public function getValueAttribute($value) {
        return json_decode($value,true);
    }

    public function setValueAttribute($value) {
        $this->attributes['value'] = json_encode($value);
    }
}
