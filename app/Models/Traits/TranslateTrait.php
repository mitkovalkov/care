<?php
namespace App\Models\Traits;

use App\Models\Translate;


trait TranslateTrait {

    public function translate() {
        return $this->morphMany(Translate::class, 'object')
            ->where('language_id', currentLangId());
    }

    public function translates() {
        return $this->morphMany(Translate::class, 'object');
    }


    private $translates = null;

    /**
     * Функция за кеширане на превода и извличането му.
     * @param $column
     * @param $lang_id
     * @return mixed|null
     */
    public function t($column, $lang_id = null) {
        if ($lang_id == null)
            $lang_id = currentLangId();
        if ($this->translates == null || !isset($this->translates[$lang_id]))
            $this->translates[$lang_id] = $this->translates()->where('language_id', $lang_id)->first();

        $t = $this->translates[$lang_id];

        if (isset($t->value)) {
            if (!is_array($t->value)) {
                $d = unserialize($t->value);
                return (key_exists($column, $d)) ? $d[$column] : null;
            }
            return (key_exists($column, $t->value)) ? $t->value[$column] : null;
        }
        return null;
    }


}
