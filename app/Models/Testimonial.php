<?php

namespace App\Models;

use App\Models\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasFactory, TranslateTrait;

    protected $fillable = ['img'];

    public function img() {
        if($this->img) {
            return getTestimonialUrl($this->img);
        }
        return null;
    }
    
    public function name($lang = null)
    {
        if (!$lang) {
            $lang = getLang(); 
        }
        return $this->t('name', $lang->id);
    }
    
    public function job($lang = null)
    {
        if (!$lang) {
            $lang = getLang(); 
        }
        return $this->t('job', $lang->id);
    }
    
    public function testimonial($lang = null)
    {
        if (!$lang) {
            $lang = getLang(); 
        }
        return $this->t('testimonial', $lang->id);
    }
}
