<?php

namespace App\Models;

use App\Models\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory, TranslateTrait;

    public function settings()
    {
        return implode("\r\n", $this->settings);
    }

    public function getSettingsAttribute($v)
    {
        return json_decode($v, true);
    }

    public function setSettingsAttribute($v)
    {
        $v = explode("\r\n", $v);
        $this->attributes['settings'] = json_encode($v);
    }

    
    public function getDataAttribute()
    {
        $data = $this->translate()->first();
        if($data) {
            return $data->value;
        }
        return [];
    }

    public function name($lang = null)
    {
        if (!$lang) {
            return '';
        }
        return $this->t('name', $lang->id);
    }

    public function setting($setting)
    {
        // #group
        // key=type;label
        $section = [];
        if (substr($setting, 0, 6) == '#group') {
            $section['content'] = 'group';
            $section['label'] = substr($setting, 7);
            
        } else if (substr($setting, 0, 9) == '#endgroup') {
            $section['content'] = 'endgroup';
            
        } else {
            $e = explode('=', $setting);
            $section['key'] = $e[0];
            $element = explode(';', $e[1]);
            if (strpos($element[0], '-')) {
                $input = explode('-', $element[0]);
                $section['content'] = $input[0];
                $section['type'] = $input[1];
            } else {
                $section['content'] = $element[0];
                $section['type'] = null;
            }
            $section['label'] = (isset($element[1])) ? $element[1] : $section['key'];
        }
        return $section;
    }
}
