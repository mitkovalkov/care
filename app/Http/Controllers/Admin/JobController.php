<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Repositories\LanguageRepository;
use App\Repositories\JobRepository;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = [];
        $page['page_title'] = "Кариери";
        $params['page'] = $page;
        $params['lang'] = LanguageRepository::findByLocale('bg');

        $params['jobs'] = JobRepository::paginate(request('page'));
        return view('admin.job.index', $params);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай Работна позиция";
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.jobs.store');
        $params['method'] = 'post';

        return view('admin.job.form', $params);
    }

    /**
     * @param \App\Http\Requests\Admin\JobStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'data.*.content' => 'required',
            ]);
        $input = $request->only(['link', 'data']);

        try {
            $job = JobRepository::create($input);
            activity_log('Job', 'store', $job->id, auth()->user()->id,  json_encode($input));

            return redirect()->route('admin.jobs.edit', $job->id)->with('success', 'Работната позиция е създадена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Job $job)
    {
        
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Job $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Job $job)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Обнови Работната позиция";
        $params['page'] = $page;
        $params['job'] = $job;
        // form
        $params['action'] = route('admin.jobs.update', $job->id);
        $params['method'] = 'PUT';

        return view('admin.job.form', $params);
    }

    public function update(Request $request, Job $job)
    {
        
        $this->validate($request, [
            'data' => 'required',
            'data.*.content' => 'required',
            ]);
        $input = $request->only(['link', 'data']);

        try {
            $job = JobRepository::update($job, $input);
            activity_log('Job', 'update', $job->id, auth()->user()->id, json_encode($input));

            return redirect()->route('admin.jobs.edit', $job->id)->with('success', 'Работната позиция е обновена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function destroy(Request $request, Job $job)
    {
        $job->translates()->delete();
        $job->delete();

        return redirect()->route('admin.jobs.index');
    }
}
