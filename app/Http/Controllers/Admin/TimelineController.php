<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Timeline;
use App\Repositories\LanguageRepository;
use App\Repositories\TimelineRepository;
use Illuminate\Http\Request;

class TimelineController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = [];
        $page['page_title'] = "Timeline";
        $params['page'] = $page;
        $params['lang'] = LanguageRepository::findByLocale('bg');

        $params['timelines'] = TimelineRepository::paginate(request('page'));
        return view('admin.timeline.index', $params);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай Timeline";
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.timelines.store');
        $params['method'] = 'post';

        return view('admin.timeline.form', $params);
    }

    /**
     * @param \App\Http\Requests\Admin\TimelineStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'data.*.content' => 'required',
            ]);
        $input = $request->only(['name', 'color', 'data']);

        try {
            $timeline = TimelineRepository::create($input);
            activity_log('Timeline', 'store', $timeline->id, auth()->user()->id,  json_encode($input));

            return redirect()->route('admin.timelines.edit', $timeline->id)->with('success', 'Timeline е създаден.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Timeline $timeline
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Timeline $timeline)
    {
        
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Timeline $timeline
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Timeline $timeline)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Обнови Timeline";
        $params['page'] = $page;
        $params['timeline'] = $timeline;
        // form
        $params['action'] = route('admin.timelines.update', $timeline->id);
        $params['method'] = 'PUT';

        return view('admin.timeline.form', $params);
    }

    public function update(Request $request, Timeline $timeline)
    {
        
        $this->validate($request, [
            'data' => 'required',
            'data.*.content' => 'required',
            ]);
        $input = $request->only(['name', 'color', 'data']);

        try {
            $timeline = TimelineRepository::update($timeline, $input);
            activity_log('Timeline', 'update', $timeline->id, auth()->user()->id, json_encode($input));

            return redirect()->route('admin.timelines.edit', $timeline->id)->with('success', 'Timeline е обновен.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function destroy(Request $request, Timeline $timeline)
    {
        $timeline->translates()->delete();
        $timeline->delete();

        return redirect()->route('admin.timelines.index');
    }
}
