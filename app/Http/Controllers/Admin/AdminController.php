<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Log;

class AdminController extends Controller
{
    public function index() {
        return view('admin.index');
    }

    public function logs()
    {
        $request = request();
        $params = [];
        $actions = [];
        if ($request->all()) {
            $actions = [
                ['link' => route('admin.logs.index'), 'anchor' => 'Back', 'type' => 'btn-primary']
            ];
        }

        $breadcrumbs = [
            'Admin' => route('admin.index'),
            'Activity log' => false,
        ];

        $page['actions'] = $actions;
        $page['breadcrumbs'] = $breadcrumbs;
        $page['page_title'] = "Log Explorer";
        $params['page'] = $page;

        $logs = Log::when(($request->input('type') != ''), function ($q) use ($request) {
            return $q->where('model_type', $request->type);
        })->when(($request->input('id') != ''), function ($q) use ($request) {
            return $q->where('model_id', $request->id);
        })->when(($request->input('user') != ''), function ($q) use ($request) {
            return $q->where('user_id', $request->user);
        })
            ->orderBy('created_at', 'desc')->paginate(paginate);

        $params['logs'] = $logs;
        return view('admin.log-index', $params);
    }

}
