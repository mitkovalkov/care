<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Repositories\LanguageRepository;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class PageController extends Controller
{
    public function index()
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Страници";
        $params['page'] = $page;
        $langs = LanguageRepository::all();

        $params['pages'] = PageRepository::paginate(request('page'));
        return view('admin.page.index', $params);
    }

    public function create()
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай страница";
        $params['page_settings'] = $page;
        // form
        $params['action'] = route('admin.pages.store');
        $params['method'] = 'post';

        return view('admin.page.form', $params);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'slug' => 'required',
            'name' => 'required',
            'settings' => 'required',
        ]);
        $input = $request->only(['slug', 'name', 'settings']);
        try {
            $page = PageRepository::create($input);
            activity_log('Page', 'store', $page->id, auth()->user()->id, "$page->slug $page->name");

            return redirect()->route('admin.pages.edit', $page->id)->with('success', 'Страницата е създадена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function show(Page $page)
    {
        //
    }
    /**
     * @param Page $page
     * @return void
     */
    public function edit(Page $page)
    {
        $params = [];
        $page_settings = [];

        $page_settings['delete'] = route('admin.pages.destroy', [$page->id]);

        $page['page_title'] = "Редактиране на страница";
        $params['page_settings'] = $page_settings;
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.pages.update', $page->id);
        $params['method'] = 'PUT';

        return view('admin.page.form', $params);
    }

    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'slug' => 'required',
            'name' => 'required',
            'settings' => 'required',
        ]);
        $input = $request->only(['slug', 'name', 'settings']);

        try {
            $page = PageRepository::update($page, $input);

            activity_log('Page', 'update', $page->id, auth()->user()->id, "$page->slug $page->name");

            return redirect()->route('admin.pages.edit', $page->id)->with('success', 'Страницата е обновена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function fill(Page $page)
    {
        $params = [];
        $params['page_settings']['page_title'] = $page->name;
        // form
        $params['action'] = route('admin.pages.fill.save', $page->id);
        $params['method'] = 'post';
        $params['page'] = $page;
        $languages = [];
        foreach ($page->translates as $translate) {
            $languages[$translate->language_id] = $translate->value;
        }
        $params['data'] = $languages;
        return view('admin.page.fill', $params);
    }
    public function fillSave(Request $request, Page $page)
    {
        $this->validate($request, [
            'data' => 'required',
        ]);
        $input = $request->only(['data']);
        try {
            foreach ($input['data'] as $lang_id => $elements) {
                foreach ($elements as $key => $element) {
                    if ($element instanceof UploadedFile) {
                        $filename = $element->getClientOriginalName();
                        $input['data'][$lang_id][$key] = storePage($element, "$page->id/$filename");
                    }
                }
            }
            $page = PageRepository::fill($page, $input);

            activity_log('Page', 'fill', $page->id, auth()->user()->id, json_encode($input));

            return redirect()->route('admin.pages.fill', $page->id)->with('success', 'Страницата е обновена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        activity_log('Page', 'destroy', $page->id, auth()->user()->id, "destroy $page->name");
        return redirect()->route('admin.pages.index')->with('success', 'Страницата е изтрита.');
    }
}
