<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Counter;
use App\Repositories\LanguageRepository;
use App\Repositories\CounterRepository;
use Illuminate\Http\Request;

class CounterController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = [];
        $page['page_title'] = "Counter";
        $params['page'] = $page;
        $params['lang'] = LanguageRepository::findByLocale('bg');

        $params['counters'] = CounterRepository::paginate(request('page'));
        return view('admin.counter.index', $params);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай Counter";
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.counters.store');
        $params['method'] = 'post';

        return view('admin.counter.form', $params);
    }

    /**
     * @param \App\Http\Requests\Admin\CounterStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'data.*.name' => 'required',
            ]);
        $input = $request->only(['img', 'color', 'data']);

        try {
            $counter = CounterRepository::create($input);
            activity_log('Counter', 'store', $counter->id, auth()->user()->id,  json_encode($input));

            return redirect()->route('admin.counters.edit', $counter->id)->with('success', 'Counter е създаден.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Counter $counter
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Counter $counter)
    {
        
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Counter $counter
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Counter $counter)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Обнови Counter";
        $params['page'] = $page;
        $params['counter'] = $counter;
        // form
        $params['action'] = route('admin.counters.update', $counter->id);
        $params['method'] = 'PUT';

        return view('admin.counter.form', $params);
    }

    public function update(Request $request, Counter $counter)
    {
        
        $this->validate($request, [
            'data' => 'required',
            'data.*.name' => 'required',
        ]);
        $input = $request->only(['img', 'color', 'data']);

        try {
            $counter = CounterRepository::update($counter, $input);
            activity_log('Counter', 'update', $counter->id, auth()->user()->id, json_encode($input));

            return redirect()->route('admin.counters.edit', $counter->id)->with('success', 'Counter е обновен.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function destroy(Request $request, Counter $counter)
    {
        $counter->translates()->delete();
        $counter->delete();

        return redirect()->route('admin.counters.index');
    }
}
