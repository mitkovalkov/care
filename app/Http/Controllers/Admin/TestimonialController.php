<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use App\Repositories\LanguageRepository;
use App\Repositories\TestimonialRepository;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = [];
        $page['page_title'] = "Препоръки";
        $params['page'] = $page;
        $params['lang'] = LanguageRepository::findByLocale('bg');

        $params['testimonials'] = TestimonialRepository::paginate(request('page'));
        return view('admin.testimonial.index', $params);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай препоръка";
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.testimonials.store');
        $params['method'] = 'post';

        return view('admin.testimonial.form', $params);
    }

    /**
     * @param \App\Http\Requests\Admin\TestimonialStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'data.*.name' => 'required',
            'data.*.job' => 'required',
            'data.*.testimonial' => 'required',
        ]);
        $input = $request->only(['img', 'data']);

        try {
            $testimonial = TestimonialRepository::create($input);
            activity_log('testimonial', 'store', $testimonial->id, auth()->user()->id,  json_encode($input));

            return redirect()->route('admin.testimonials.edit', $testimonial->id)->with('success', 'Препоръката е създадена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Testimonial $testimonial)
    {
        
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Testimonial $testimonial)
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Обнови препоръка";
        $params['page'] = $page;
        $params['testimonial'] = $testimonial;
        // form
        $params['action'] = route('admin.testimonials.update', $testimonial->id);
        $params['method'] = 'PUT';

        return view('admin.testimonial.form', $params);
    }

    public function update(Request $request, Testimonial $testimonial)
    {
        
        $this->validate($request, [
            'data' => 'required',
            'data.*.name' => 'required',
            'data.*.job' => 'required',
            'data.*.testimonial' => 'required',
        ]);
        $input = $request->only(['img', 'data']);

        try {
            $testimonial = TestimonialRepository::update($testimonial, $input);
            activity_log('testimonial', 'update', $testimonial->id, auth()->user()->id, json_encode($input));

            return redirect()->route('admin.testimonials.edit', $testimonial->id)->with('success', 'Препоръката е обновена.');
        } catch (\Exception $e) {
            if (config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    public function destroy(Request $request, Testimonial $testimonial)
    {
        $testimonial->translates()->delete();
        $testimonial->delete();

        return redirect()->route('admin.testimonials.index');
    }
}
