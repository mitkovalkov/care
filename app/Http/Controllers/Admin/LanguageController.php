<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Repositories\LanguageRepository;
use Illuminate\Http\Request;
use Prologue\Alerts\Facades\Alert;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Езици";
        $params['page'] = $page;
        $params['languages'] = LanguageRepository::paginate(request('page'));
        return view('admin.language.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [];
        $page = [];

        $page['page_title'] = "Създай език";
        $params['page'] = $page;
        // form
        $params['action'] = route('admin.langs.store');
        $params['method'] = 'post';

        return view('admin.language.form', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'locale' => 'required',
            ]);
        $input = $request->only(['name', 'locale', 'flag']);
        
        try {
            $language = LanguageRepository::create($input);
            activity_log('Language','store',$language->id, auth()->user()->id, "$language->flag $language->name");
            
            return redirect()->route('admin.langs.edit', $language->id)->with('success', 'Езикът е създаден.');
        } catch(\Exception $e) {
            if(config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $lang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $lang)
    {
        $params = [];
        $page = [];

        $page['delete'] = route('admin.langs.destroy', [$lang->id]);

        $page['page_title'] = "Редактирай език";
        $params['page'] = $page;
        $params['language'] = $lang;
        // form
        $params['action'] = route('admin.langs.update', $lang->id);
        $params['method'] = 'PUT';


        return view('admin.language.form', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $lang)
    {
        $this->validate($request, [
            'name' => 'required',
            ]);
        $input = $request->only(['name','flag']);
        
        try {
            $language = LanguageRepository::update($lang, $input);

            activity_log('Language','update',$language->id, auth()->user()->id, "$language->flag $language->name");
            
            return redirect()->route('admin.langs.edit', $language->id)->with('success', 'Езикът е обновен.');
        } catch(\Exception $e) {
            if(config('app.debug')) {
                dd($e);
            }
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $lang)
    {
        $lang->delete();

        activity_log('Language','destroy',$lang->id, auth()->user()->id, "destroy $lang->name");
        return redirect()->route('admin.langs.index')->with('success', 'Езикът е изтрит.');
    }
}
