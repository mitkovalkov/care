<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Repositories\TestimonialRepository;
use App\Repositories\CounterRepository;
use App\Repositories\JobRepository;
use App\Repositories\TimelineRepository;

class FrontController extends Controller
{
    public function show($locale, $slug)
    {
        $page = page($slug);
        // dd($page);
        return view('pages.simple-page', $page);
    }

    public function indexPage()
    {
        $params = page('index');
        $params['testimonials'] = TestimonialRepository::all();
        $params['counters'] = CounterRepository::all();
        return view('pages.homepage', $params);
    }

    
    public function jobsIndex()
    {
        $params = [];
        $params['jobs'] = JobRepository::all();
        return view('jobs.index', $params);
    }

    public function jobsShow($lang, Job $job)
    {
        $params = [];
        $params['job'] = $job;
        return view('jobs.show', $params);
    }

    public function pspPage()
    {
        $params = page('patient-support-program');
        $params['testimonials'] = TestimonialRepository::all();
        return view('pages.inner-page', $params);
    }

    public function homeCare()
    {
        $params = page('homecare');
        return view('pages.homecare', $params);
    }

    public function covidPage()
    {
        $params = page('covid-19');
        return view('pages.covid', $params);
    }

    public function postCovidPage()
    {
        $params = page('post-covid');
        return view('pages.post-covid', $params);
    }

    public function cardiologyPage()
    {
        $params = page('cardiology');
        return view('pages.cardiology', $params);
    }
    
    public function companyPage()
    {
        $params = page('company');
        $params['timeline'] = TimelineRepository::all();
        return view('pages.about-us', $params);
    }
}
