<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Language;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'admin@admin.bg';
        $user->name = 'Admin Account';
        $user->password = bcrypt('123456789');
        $user->acc_type = 10;
        $user->save();
        echo "Админ създаден \n";

        
        $lagns = [];
        $lagns['bg'] = 'Български';
        $lagns['en'] = 'Английски';
        foreach($lagns as $locale => $name) {
            $c = new Language();
            $c->name = $name;
            $c->locale = $locale;
            $c->save();
            echo "$name добавен\n";
        }
    }
}
