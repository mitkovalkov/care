<section class="section section-padding section-cta section-padding-sm">
    <div class="container">
        <div class="flex-row">
            <div class="flex-box flex-column">
                @if(isset($title))
                <div class="title-container">
                    <h2 class="main-title animated-item-bottom">{{ $title }}</h2>
                </div>
                @endif
                @if(isset($text))
                <div class="text company-text animated-item-bottom">
                    {!! $text !!}
                </div>
                @endif
                @if(isset($btnText))
                <div class="button-container animated-item-bottom">
                    <a href="{{ r('covid-19') }}" class="btn">{{ $btnText }}</a>
                </div>
                @endif
            </div>
            @if(isset($image))
            <div class="flex-box flex-column">
                <div class="img-box animated-item-bottom"><img src="{{ $image }}" alt="@if(isset($title)) {{ $title }} @endif"></div>
            </div>
            @endif
        </div>
    </div>
</section>
