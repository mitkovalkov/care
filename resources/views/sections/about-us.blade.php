<div class="section section-about-us">
    <div class="container container-big">
        <div class="flex-row ai-center">
            <div class="flex-box">
                <div class="images-container animation-transform" data-bottom-top="transform:translate3d(0px, 70px, 0px)" data-top-bottom="transform:translate3d(0px, -70px, 0px)">
                    <div class="img-box animated-item"><img src="{{ $image1 }}" alt="Care by SAT Health"></div>
                    <div class="img-box animated-item"><img src="{{ $image2 }}" alt="Care by SAT Health"></div>
                    <div class="img-box animated-item"><img src="{{ $image3 }}" alt="Care by SAT Health"></div>
                    <div class="img-box animated-item"><img src="{{ $image4 }}" alt="Care by SAT Health"></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="flex-box-content">
                    <div class="title-container animation-transform" data-bottom-top="transform:translate3d(0px, -70px, 0px)" data-top-bottom="transform:translate3d(0px, 70px, 0px)">
                        <h2 class="main-title animated-item-bottom">{{ $title }}</h2>
                        <div class="text animated-item-bottom">
                            {!! $text !!}
                        </div>
                        @if(isset($btn_url))
                        <div class="button-container animated-item-bottom">
                            <a href="{{ $btn_url }}" class="btn">{{ $btn_text }}</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>