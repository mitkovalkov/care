<div class="section section-counter section-gray-bg">
    <div class="container">
        <div class="title-container title-center">
            <h2 class="main-title animated-item-bottom">{{ trans('common.counter_title') }}</h2>
        </div>
        <div class="counter-grid">
            @foreach($counters as $item)
            <div class="counter-item animated-item">
                <div class="counter-info">
                    <div class="counter-icon"></div>
                    @if($item->content())
                    <div class="flex-row ai-center jc-sb">
                        <div class="counter-number" data-number="{{ $item->val() }}">{{ $item->val() }}</div>
                        <div class="counter-type ityped" data-itype="{{ $item->content() }}"></div>
                    </div>
                    <div class="counter-title">{!! $item->name() !!}</div>
                    @else
                    <div class="counter-number" data-number="{{ $item->val() }}">{{ $item->val() }}</div>
                    <div class="counter-title">{!! $item->name() !!}</div>
                    @endif
                </div>
                <div class="counter-image"><img src="{{ $item->img() }}" alt=""></div>
            </div>
            @endforeach
        </div>
        <div class="swiper-buttons">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
</div>