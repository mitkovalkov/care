<div class="section section-pharma" id="section2">
    <div class="container">
        <div class="title-container">
            <h2 class="main-title animated-item-bottom">{!! $title !!}</h2>
        </div>
        <div class="flex-row">
            <div class="flex-box">
                <div class="img-box animated-item"><img src="{{ $image }}" alt="{{ $title }}"></div>
            </div>
            <div class="flex-box">
                <div class="text animated-item">
                    {!! $text !!}
                </div>
                @if(isset($button_text))
                <div class="btn-declaration animated-item-bottom">
                    <div class="btn show-popup" data-popup="{!! $modal_text !!}">{{ $button_text }}</div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>