<div class="section section-title">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="main-title animated-item-bottom">{!! $title !!}</h2>
        </div>
    </div>
</div>
