<div class="section section-programs @if($section_gray) section-gray-bg @endif">
    <div class="container">
        <div class="programs-listing">
            <div class="program-item">
                <div class="flex-row ai-center">
                    <div class="flex-box">
                        <div class="title-container" data-aos="fade-right" data-aos-delay="300">
                            <h2 class="main-title">{{ $title }}</h2>
                            <div class="text">
                                {!! $text !!}
                            </div>
                            @if($button)
                            <div class="button-container">
                                <a href="{{ r('inner-page') }}" class="btn">{{ trans('common.learn_more') }}</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="flex-box">
                        <div class="img-box" data-aos="fade-left" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="200">
                            <img src="{{ $image }}" alt="{{ $title }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>