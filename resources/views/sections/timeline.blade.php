<div class="section section-timeline">
    <div class="container">
        <div class="title-container">
            <h2 class="main-title animated-item-bottom">{{ trans('common.timeline') }}</h2>
        </div>
        <div class="timeline-content animated-item">
            @foreach($timeline as $key => $item)
                <div class="timeline-item @if($key === 0) is-active @endif" data-color="{{ $item->color }}">
                    <div class="timeline-title">{{ $item->name }}</div>
                    <div class="timeline-item-info">
                        @php
                            $content = $item->content();
                            if (preg_match_all('%(<p[^>]*>.*?</p>)%i', $content, $regs)) {
                                if(is_array($regs)) {
                                    foreach($regs[0] as $reg) {
                                        $p_content = explode(', ', strip_tags($reg));
                                        echo '
                                        <div class="timeline-counter">
                                            <div class="timeline-counter-number" data-number="'.$p_content[0].'">'.(!empty($p_content[0]) ? $p_content[0] : '').'</div>
                                            <div class="timeline-counter-title">'.(!empty($p_content[1]) ? $p_content[1] : '').'</div>
                                        </div>';
                                    }
                                }
                                $result = $regs[1];
                            } else {
                                $result = "";
                            }
                        @endphp
                    </div>
                </div>
            @endforeach
            <!-- <div class="timeline-item is-active">
                <div class="timeline-item-info">
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Clients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="35">35</div>
                        <div class="timeline-counter-title">Patients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Therapy Areas</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="2">2</div>
                        <div class="timeline-counter-title">Patient Support Assistants</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Cordinators</div>
                    </div>
                </div>
                <div class="timeline-title">2018</div>
            </div>
            <div class="timeline-item">
                <div class="timeline-item-info">
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="3">3</div>
                        <div class="timeline-counter-title">Clients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="970">970</div>
                        <div class="timeline-counter-title">Patients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="5">5</div>
                        <div class="timeline-counter-title">Therapy Areas</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="9">9</div>
                        <div class="timeline-counter-title">Patient Support Assistants</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Manager</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Cordinators</div>
                    </div>
                </div>
                <div class="timeline-title">2019</div>
            </div>
            <div class="timeline-item">
                <div class="timeline-item-info">
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="3">5</div>
                        <div class="timeline-counter-title">Clients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="2979">2979</div>
                        <div class="timeline-counter-title">Patients</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="7">7</div>
                        <div class="timeline-counter-title">Therapy Areas</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="15">15</div>
                        <div class="timeline-counter-title">Patient Support Assistants</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Manager</div>
                    </div>
                    <div class="timeline-counter">
                        <div class="timeline-counter-number" data-number="1">1</div>
                        <div class="timeline-counter-title">Cordinators</div>
                    </div>
                </div>
                <div class="timeline-title">2020</div>
            </div> -->
        </div>
    </div>
</div>