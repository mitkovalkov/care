<div class="section section-video">
    <div class="flex-row ai-stretch-center">
        <div class="video-box">
            <div class="close-video"><div class="icon icon-close"></div>{{ trans('common.close') }}</div>
            <video poster="{{ asset('images/video/SatHealt_video.jpg') }}" id="video" class="video" loop="loop" autoplay="autoplay" muted playsinline title="Care by SAT Health">
                <source src="{{ asset('images/video/SatHealt_video_new.m4v') }}" type="video/mp4">
                <source src="{{ asset('images/video/SatHealt_video_new.webm') }}" type="video/webm">
                <source src="{{ asset('images/video/SatHealt_video_new.ogv') }}" type="video/ogg">
                <source src="{{ asset('images/video/SatHealt_video_new.mp4') }}">
            </video>
        </div>
        <div class="video-information-content">
            <div class="container title-container">
                <h1 class="main-title animated-item">{!! $data['video_title'] !!}</h1>
                <div class="text animated-item-bottom"><p>{{ $data['video_text'] }}</p></div>
                <div class="button-container animated-item-bottom">
                    <div class="btn" id="js-play-video">{{ trans('common.view_full_video') }}</div>
                </div>
            </div>
        </div>
    </div>
</div>