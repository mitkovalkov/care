<div class="section section-gray-bg section-company-info @if($video) has-video @endif" id="section1">
    <div class="container">
        <div class="title-container">
            <h2 class="main-title animated-item-bottom">{{ $title }}</h2>
        </div>
        <div class="text company-text animated-item-bottom">
            {!! $text !!}
        </div>
        @if($video)
        <div class="video-section">
            <div class="video-box">
                <div id="js-play-button" class="play-btn animated-item-bottom"></div>
                <video poster="{{ asset('images/video/SatHealt_video.jpg') }}" id="video" class="video animated-item-bottom" loop="loop" title="Care by SAT Health">
                    <source src="{{ asset('images/video/SatHealt_video_new.m4v') }}" type="video/mp4">
                    <source src="{{ asset('images/video/SatHealt_video_new.webm') }}" type="video/webm">
                    <source src="{{ asset('images/video/SatHealt_video_new.ogv') }}" type="video/ogg">
                    <source src="{{ asset('images/video/SatHealt_video_new.mp4') }}">
                </video>
            </div>
        </div>
        @endif
    </div>
</div>