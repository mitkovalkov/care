<div class="section section-our-mission">
    <div class="container">
        <div class="title-container title-center">
            <h2 class="main-title animated-item-bottom">{{ $data['mission_title'] }}</h2>
            <div class="text animated-item-bottom">
                <p>{{ $data['mission_text'] }}</p>
            </div>
        </div>
    </div>
</div>
