<section class="section section-covid" @if(isset($image)) style="background-image: url('{{ $image }}');" @endif>
    <div class="container">
        @if(isset($text))
            <div class="text company-text animated-item-bottom">
                {!! $text !!}
            </div>
        @endif
    </div>
</section>