<div class="section section-services" id="section3">
    <div class="container">
        <div class="title-container">
            <h3 class="main-title animated-item-bottom">{{ $data['service_title'] }}</h3>
            <div class="text animated-item">{!! $data['service_text'] !!}</div>
        </div>
        <div class="swiper-container swiper-services animated-item">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_1_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_1_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_2_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_2_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_3_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_3_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_4_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_4_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_5_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_5_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_6_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_6_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_7_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_7_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_8_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_8_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_9_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_9_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_10_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_10_title'] }}</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="single-service animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['service_11_image']) }}" alt=""></div>
                        <div class="service-caption">{{ $data['service_11_title'] }}</div>
                    </div>
                </div>
            </div>
            <div class="swiper-buttons">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</div>