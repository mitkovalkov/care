<div class="section section-active-progrmas">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="main-title animated-item-bottom">{!! $title !!}</h2>
        </div>
        <div class="text text-center animated-item-bottom">
            {!! $text !!}
        </div>
        <div class="active-programs swiper-services swiper-container">
            <div class="swiper-wrapper">
                @foreach($active_programs as $program)
                    @if($program['title'])
                    <div class="swiper-slide active-program animated-item-bottom">
                        <a href="{{ $program['url'] ? $program['url'] : '#!' }}">
                            <div class="img-box"><img src="{{ getPageUrl($program['image']) }}" alt="{{ $program['title'] }}"></div>
                            <div class="active-program-title">{!! $program['title'] !!}</div>
                            <div class="active-pgoram-text text">{!! $program['text'] !!}</p></div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="swiper-buttons">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</div>
