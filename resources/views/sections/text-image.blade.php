<div class="section-our-approach" id="section1">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="main-title"  data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">{!! $data['page_title'] !!}</h2>
        </div>
    </div>
    <div class="section-text-image">
        <div class="section flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="icon animated-item-bottom"><img src="{{ getPageUrl($data['section_1_icon']) }}" alt=""></div>
                    <h3 class="title animated-item-bottom">{{ $data['section_1_title'] }}</h3>
                    <div class="text animated-item-bottom">
                        {!! $data['section_1_text'] !!}
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="img-box" data-aos="fade-left" data-aos-delay="600"><img src="{{ getPageUrl($data['section_1_image']) }}" alt=""></div>
            </div>
        </div>
        <div class="section flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="icon animated-item-bottom"><img src="{{ getPageUrl($data['section_2_icon']) }}" alt=""></div>
                    <h3 class="title animated-item-bottom">{{ $data['section_2_title'] }}</h3>
                    <div class="text animated-item-bottom">
                        {!! $data['section_2_text'] !!}
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="img-box" data-aos="fade-right" data-aos-delay="600"><img src="{{ getPageUrl($data['section_2_image']) }}" alt=""></div>
            </div>
        </div>
        <div class="section flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="icon animated-item-bottom"><img src="{{ getPageUrl($data['section_3_icon']) }}" alt=""></div>
                    <h3 class="title animated-item-bottom">{{ $data['section_3_title'] }}</h3>
                    <div class="text animated-item-bottom">
                        {!! $data['section_3_text'] !!}
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="img-box" data-aos="fade-right" data-aos-delay="600"><img src="{{ getPageUrl($data['section_3_image']) }}" alt=""></div>
            </div>
        </div>
        <div class="section flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="icon animated-item-bottom"><img src="{{ getPageUrl($data['section_4_icon']) }}" alt=""></div>
                    <h3 class="title animated-item-bottom">{{ $data['section_4_title'] }}</h3>
                    <div class="text animated-item-bottom">
                        {!! $data['section_4_text'] !!}
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="img-box" data-aos="fade-right" data-aos-delay="600"><img src="{{ getPageUrl($data['section_4_image']) }}" alt=""></div>
            </div>
        </div>
        <div class="section flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="icon animated-item-bottom"><img src="{{ getPageUrl($data['section_5_icon']) }}" alt=""></div>
                    <h3 class="title animated-item-bottom">{{ $data['section_5_title'] }}</h3>
                    <div class="text animated-item-bottom">
                        {!! $data['section_5_text'] !!}
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="img-box" data-aos="fade-right" data-aos-delay="600"><img src="{{ getPageUrl($data['section_5_image']) }}" alt=""></div>
            </div>
        </div>
    </div>
</div>