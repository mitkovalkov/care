<div class="section section-icons @if($section_bg) {{ $section_bg }} @endif">
    <div class="container">
        <div class="title-container text-center">
            <h3 class="main-title animated-item-bottom">{!! $title !!}</h3>
        </div>
        <div class="section-container">
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_1 }}" alt="{{ $homecare_advantages_title_1 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_1 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_2 }}" alt="{{ $homecare_advantages_title_2 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_2 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_3 }}" alt="{{ $homecare_advantages_title_3 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_3 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_4 }}" alt="{{ $homecare_advantages_title_4 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_4 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_5 }}" alt="{{ $homecare_advantages_title_5 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_5 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_6 }}" alt="{{ $homecare_advantages_title_6 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_6 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_7 }}" alt="{{ $homecare_advantages_title_7 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_7 }}</p>
                </div>
            </div>
            <div class="item-content animated-item-bottom">
                <div class="img-box"><img src="{{ $homecare_advantages_image_8 }}" alt="{{ $homecare_advantages_title_8 }}"></div>
                <div class="text text-center">
                    <p>{{ $homecare_advantages_title_8 }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
