<div class="section section-clients" id="section3">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="main-title animated-item-bottom">{{ trans('common.partners_title') }}</h2>
            <div class="text animated-item"><p>{!! trans('common.partners_text') !!}</p></div>
        </div>
        <div class="flex-row flex-wrap">
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/Mylan.png') }}" alt=""></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/Roche.png') }}" alt=""></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/Takeda.png') }}" alt=""></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/astellas.png') }}" alt=""></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/AZ_LOGO.png') }}" alt=""></div>
                </div>
            </div>
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ asset('images/clients/Johnson.png') }}" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</div>