<div class="section section-carousel-text section-gray-bg" id="section2">
    <div class="container">
        <div class="title-container">
            <h2 class="main-title animated-item-bottom">{{ trans('navigation.our_approach') }}</h2>
        </div>
        <div class="swiper-container swiper-text animated-item">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="flex-row">
                        <div class="flex-box">
                            <div class="text-content">
                                <div class="title-container">
                                    <h3 class="main-title">{{ $data['approach_1_title'] }}</h3>
                                    <div class="text">
                                        {!! $data['approach_1_text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-box"><div class="img-box"><img src="{{ getPageUrl($data['approach_1_image']) }}" alt=""></div></div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="flex-row">
                        <div class="flex-box">
                            <div class="text-content">
                                <div class="title-container">
                                    <h3 class="main-title">{{ $data['approach_2_title'] }}</h3>
                                    <div class="text">
                                        {!! $data['approach_2_text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-box"><div class="img-box"><img src="{{ getPageUrl($data['approach_2_image']) }}" alt=""></div></div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="flex-row">
                        <div class="flex-box">
                            <div class="text-content">
                                <div class="title-container">
                                    <h3 class="main-title">{{ $data['approach_3_title'] }}</h3>
                                    <div class="text">
                                        {!! $data['approach_3_text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-box"><div class="img-box"><img src="{{ getPageUrl($data['approach_3_image']) }}" alt=""></div></div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="flex-row">
                        <div class="flex-box">
                            <div class="text-content">
                                <div class="title-container">
                                    <h3 class="main-title">{{ $data['approach_4_title'] }}</h3>
                                    <div class="text">
                                        {!! $data['approach_4_text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-box"><div class="img-box"><img src="{{ getPageUrl($data['approach_4_image']) }}" alt=""></div></div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="flex-row">
                        <div class="flex-box">
                            <div class="text-content">
                                <div class="title-container">
                                    <h3 class="main-title">{{ $data['approach_5_title'] }}</h3>
                                    <div class="text">
                                        {!! $data['approach_5_text'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-box"><div class="img-box"><img src="{{ getPageUrl($data['approach_5_image']) }}" alt=""></div></div>
                    </div>
                </div>
            </div>
            <div class="swiper-buttons">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</div>