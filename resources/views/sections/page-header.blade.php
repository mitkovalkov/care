<div class="section section-page-header @if(!isset($image)) no-image @endif">
    @if(isset($image))<div class="img-box"><img src="{{ $image ?? '' }}" alt=""></div>@endif
    <div class="page-caption">
        <div class="container">
            <h1 class="page-title animated-item-bottom">{!! $title !!}</h1>
            @if(isset($text))<div class="text animated-item-bottom"><p>{!! $text !!}</p></div>@endif
            @if(isset($btn))
            <div class="btn-container animated-item-bottom">
                <a href="{{ $btn['link'] }}" class="btn @if(isset($btn['class'])) {{ $btn['class'] }} @else btn-blue @endif">{{ $btn['name'] }}</a>
            </div>
            @endif
        </div>
    </div>
</div>