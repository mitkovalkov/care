<div class="section section-clients">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="main-title animated-item-bottom">{{ $title }}</h2>
            <div class="text animated-item"><p>{!! $text !!}</p></div>
        </div>
        <div class="flex-row flex-wrap">
            <div class="flex-box">
                <div class="client-item animated-item">
                    <div class="img-box"><img src="{{ $image }}" alt="{{ $title }}"></div>
                </div>
            </div>
        </div>
        <div class="embed-video">
            <video poster="{{ asset('images/video/CHECKPOINT.jpg') }}" controls title="Care by SAT Health" width="100%">
                <source src="{{ asset('images/video/CHECKPOINT.m4v') }}" type="video/mp4">
                <source src="{{ asset('images/video/CHECKPOINT.webm') }}" type="video/webm">
                <source src="{{ asset('images/video/CHECKPOINT.ogv') }}" type="video/ogg">
                <source src="{{ asset('images/video/CHECKPOINT.mp4') }}">
            </video>
        </div>
        <div class="button-container mt-20 mb-20 justiy-content-center">
            <a href="{{ trans('partials.btn_video_url') }}" class="btn" target="_blank">{{ trans('partials.btn_video_text') }}</a>
        </div>
    </div>
</div>