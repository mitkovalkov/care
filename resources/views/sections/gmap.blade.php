<div class="section-gmap">
    <div id="google_map" data-lat="{{ trans('contacts.latitude') }}" data-long="{{ trans('contacts.longitude') }}" data-infowindow="{{ trans('contacts.map_popup_text') }}"></div>
</div>

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd4VleJb2OJg2w-4AtlYepY2Lk9cnOD4k&callback=initGoogleMap"></script>
<script>
    function initGoogleMap() {
        var latitude = document.getElementById('google_map').getAttribute('data-lat');
        var longitude = document.getElementById('google_map').getAttribute('data-long');
        var infoWindowText = document.getElementById('google_map').getAttribute('data-infowindow');
        var myOptions = {
            zoom: 15,
            scrollwheel: false,
            center: new google.maps.LatLng(Number(latitude), Number(longitude)),
            // styles: mapStyle,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        }
        var map = new google.maps.Map(document.getElementById('google_map'), myOptions)
        
        var infowindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(Number(latitude), Number(longitude)),
            icon: '/images/pin.png',
            map: map
        });
    
        google.maps.event.addListener(marker, 'click', (function(marker) {
            return function() {
                infowindow.setContent(infoWindowText);
                infowindow.open(map, marker);
            }
        })(marker));
    }

    window.initGoogleMap = initGoogleMap
</script>
@endsection