@if($testimonials)
<div class="section section-testimonials section-gray-bg">
    <div class="container">
        <div class="swiper-inventar animated-item-bottom">
            <div class="title-container">
                <h2 class="main-title">{{ trans('common.testimonials') }}</h2>
            </div>
        </div>
        <div class="swiper-container swiper-testimonials animated-item-bottom">
            <div class="swiper-wrapper">
                @foreach($testimonials as $item)
                <div class="swiper-slide">
                    <div class="testimonial-item">
                        <div class="img-box"><img src="{{ $item->img() }}" alt=""></div>
                        <div class="testimonial-content">
                            <div class="testimonial-text">
                                <p>{{ $item->testimonial() }}</p>
                            </div>
                            <div class="testimonial-title">{{ $item->name() }} <span>{{ $item->job() }}</span></div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="swiper-buttons">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
</div>
@endif