<div class="section section-contacts-info">
    <div class="container">
        <div class="flex-row flex-wrap">
            <div class="flex-box">
                <div class="contacts-item animated-item-bottom">
                    <div class="title">{{ trans('contacts.phone') }}</div>
                    <a href="tel:{{ trans('contacts.phone_number') }}" class="phone">{{ trans('contacts.phone_text') }}</a>
                </div>
            </div>
            <div class="flex-box">
                <div class="contacts-item animated-item-bottom">
                    <div class="title">{{ trans('contacts.head_office') }}</div>
                    <div class="text">
                        <p>{!! trans('contacts.address') !!}</p>
                    </div>
                </div>
            </div>
            <div class="flex-box">
                <div class="contacts-item animated-item-bottom">
                    <div class="title">{{ trans('contacts.email_label') }}</div>
                    <a href="mailto:{{ trans('contacts.email') }}" class="email">{{ trans('contacts.email') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>