<div class="section section-our-team section-gray-bg" id="section4">
    <div class="container">
        <div class="flex-row ai-center">
            <div class="flex-box">
                <div class="text-content">
                    <div class="title-container">
                        <h3 class="main-title animated-item-bottom">{!! $title !!}</h3>
                        <div class="text animated-item-bottom">
                            {!! $text !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex-box"><div class="img-box" data-aos="fade-left" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="200"><img src="{{ getPageUrl($data['team_company_image']) }}" alt=""></div></div>
        </div>
        {{--
        <div class="team-members">
            <div class="title-container text-center">
                <h2 class="main-title">{{ $team_title }}</h2>
            </div>
            <div class="flex-row flex-wrap">
                <div class="flex-box">
                    <div class="team-member animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['team_member_1_image']) }}" alt=""></div>
                        <div class="team-member-info">
                            <div class="box">
                                <div class="team-member-name">{{ $data['team_member_1_title'] }}</div>
                                <div class="team-member-position">{{ $data['team_member_1_text'] }}</div>
                            </div>
                            @if($data['team_member_1_linkedin'])
                            <div class="social-links">
                                <a href="{{ $data['team_member_1_linkedin'] }}" class="social-insta"></a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="flex-box">
                    <div class="team-member animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['team_member_2_image']) }}" alt=""></div>
                        <div class="team-member-info">
                            <div class="box">
                                <div class="team-member-name">{{ $data['team_member_2_title'] }}</div>
                                <div class="team-member-position">{{ $data['team_member_2_text'] }}</div>
                            </div>
                            @if($data['team_member_1_linkedin'])
                            <div class="social-links">
                                <a href="{{ $data['team_member_2_linkedin'] }}" class="social-insta"></a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="flex-box">
                    <div class="team-member animated-item">
                        <div class="img-box"><img src="{{ getPageUrl($data['team_member_3_image']) }}" alt=""></div>
                        <div class="team-member-info">
                            <div class="box">
                                <div class="team-member-name">{{ $data['team_member_3_title'] }}</div>
                                <div class="team-member-position">{{ $data['team_member_3_text'] }}</div>
                            </div>
                            @if($data['team_member_1_linkedin'])
                            <div class="social-links">
                                <a href="{{ $data['team_member_3_linkedin'] }}" class="social-insta"></a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --}}
    </div>
</div>