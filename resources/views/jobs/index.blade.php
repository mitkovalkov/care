@extends('layouts.main')

@section('content')
@include('sections.page-header', ['title' => trans('common.careers_title'), 'text' => trans('common.careers_description')])
<div class="section section-jobs padding-lg section-bg-gray">
    <div class="container">
        @if(count($jobs))
            @foreach($jobs as $key => $job)
                <div class="accordion animated-item-bottom">
                    <div class="accordion-title">{{ $job->name() }} <div class="icon icon-next"></div></div>
                    <div class="accordion-content">
                        <div class="text">
                            {!! $job->content() !!}
                        </div>
                        @if($job->link)
                        <div class="btn-container">
                            <a href="{{ $job->link }}" class="apply-now" target="_blank">{{ trans('common.apply_now') }} <div class="icon icon-next"></div></a>
                        </div>
                        @endif
                    </div>
                </div>
            @endforeach
        @else
            <h2 class="text-center animated-item-bottom">{{ trans('common.no_jobs_text') }}</h2>
        @endif
    </div>
</div>
@endsection