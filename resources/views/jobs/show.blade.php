@extends('layouts.main')

@section('content')
@include('sections.page-header', ['title' => $job->name(), 'image' => @asset('images/careers-bg.jpg')])
<div class="section section-jobs padding-lg section-bg-gray">
    <div class="container">
        <div class="text">{!! $job->content() !!}</div>
        @if($job->link)
        <a href="{{ $job->link }}" class="apply-now" target="_blank">{{ trans('common.apply_now') }} <div class="icon icon-next"></div></a>
        @endif
    </div>
</div>
@endsection
