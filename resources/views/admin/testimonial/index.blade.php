<x-admin :page="$page">
    <x-table :columns="['Име', 'Длъжност', 'Действия']">
        @foreach($testimonials as $testimonial)
        <tr>
            <td>{{ $testimonial->name($lang) }}</td>
            <td>{{ $testimonial->job($lang) }}</td>
            <td>
                <a href="{{ route('admin.testimonials.edit', $testimonial->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Редактирай</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$testimonials />
</x-admin>