<x-admin :page=$page>
    <x-form.form :action="$action" :method="$method" :upload="true">
        <div class="w-100">
            @isset($testimonial->img)
            <img src="{{ $testimonial->img() }}" alt="photo">
            @endisset
            <x-form.upload name="img" label="Photo" />
            <ul class="nav nav-tabs">
                @foreach (langs() as $i => $lang)
                <li class="nav-item">
                    <a class="nav-link border-left-0 @if($i == 0) active @endif" id="form-locale-{{ $lang->locale }}"
                        data-toggle="tab" href="#form_{{ $lang->locale }}" role="tab">{{ $lang->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="forms">
                @foreach (langs() as $lang)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="form_{{ $lang->locale }}"
                    role="tabpanel">
                    <div class="row px-3">
                        <x-form.input name="data[{{ $lang->id }}][name]" label="Име" required size=6
                            value="{{ old('data.' . $lang->id . '.name', isset($testimonial) ? $testimonial->name($lang) : '') }}" />

                        <x-form.input name="data[{{ $lang->id }}][job]" label="Длъжност" required size=6
                            value="{{ old('data.' . $lang->id . '.job', isset($testimonial) ? $testimonial->job($lang) : '') }}" />
                    </div>


                    <x-form.textarea name="data[{{ $lang->id }}][testimonial]" label="Препoръка">
                        {{ old('data.' . $lang->id . '.testimonial', isset($testimonial) ? $testimonial->testimonial($lang) : '') }}</x-form.textarea>
                </div>
                @endforeach
            </div>

        </div>
    </x-form.form>
</x-admin>