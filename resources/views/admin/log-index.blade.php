<x-admin :page="$page">
    <div class="table-responsive">
        <table class="table bg-white">
            <tr>
                <th>Resource</th>
                <th>Resource id</th>
                <th>Action</th>
                <th>User</th>
                <th>Date</th>
                <th>Message</th>
            </tr>
            @foreach($logs as $log)
            <tr>
                <td>
                    <a href="{{ route('admin.logs.index', ['type'=> $log->model_type]) }}">{{ $log->model_type }}</a>
                </td>
                <td>
                    <a
                        href="{{ route('admin.logs.index', ['type'=> $log->model_type, 'id'=> $log->model_id]) }}">{{ $log->model_id }}</a>
                </td>
                <td>{{ $log->method }}</td>
                <td>
                    @if($log->user)
                    <a href="{{ route('admin.logs.index', ['user' => $log->user_id]) }}">{{ $log->user->email }}</a>
                    @endif
                </td>
                <td>{{ $log->created_at }}</td>
                <td>
                    <pre><?php
                            $message = @unserialize($log->message);
                            if ($message) {
                                var_dump($message);
                            } else {
                                echo $log->message;
                            }
                        ?></pre>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    <div class="d-flex justify-content-center">
        {!! $logs->appends(request()->except('page'))->links() !!}
    </div>
</x-admin>