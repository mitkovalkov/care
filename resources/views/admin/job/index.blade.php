<x-admin :page="$page">
    <x-table :columns="['Позиция', 'Действия']">
        @foreach($jobs as $job)
        <tr>
            <td>{{ $job->name() }}</td>
            <td>
                <a href="{{ route('admin.jobs.edit', $job->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Редактирай</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$jobs />
</x-admin>