<x-admin :page=$page>
    @isset($job)
    <form method="post" action="{{ route('admin.jobs.destroy', $job->id) }}"
        onsubmit="return confirm('Изтриване на ресурса?');" class="mb-3">
        @csrf
        @method('delete')
        <button type="submit" class="btn btn-danger">Изтрий</button>
    </form>
    @endisset
    <x-form.form :action="$action" :method="$method" :upload="true">
        <div class="w-100">
            <div class="row px-3">
                <x-form.input name="link" label="Линк" value="{{ old('link', isset($job) ? $job->link : '') }}" />
            </div>
            <ul class="nav nav-tabs">
                @foreach (langs() as $i => $lang)
                <li class="nav-item">
                    <a class="nav-link border-left-0 @if($i == 0) active @endif" id="form-locale-{{ $lang->locale }}"
                        data-toggle="tab" href="#form_{{ $lang->locale }}" role="tab">{{ $lang->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="forms">
                @foreach (langs() as $lang)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="form_{{ $lang->locale }}"
                    role="tabpanel">

                    <x-form.input name="data[{{ $lang->id }}][name]" label="Позиция" value="{{ old('name', isset($job) ? $job->name($lang) : '') }}" />
                    <x-form.textarea name="data[{{ $lang->id }}][content]" label="Съдържание" class="text-editor" required>
                        {{ old('data.' . $lang->id . '.content', isset($job) ? $job->content($lang) : '') }}
                    </x-form.textarea>
                </div>
                @endforeach
            </div>

        </div>
    </x-form.form>
</x-admin>