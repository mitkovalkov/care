<x-admin :page=$page>
    <x-form.form :action="$action" :method="$method" :upload="true">
        <div class="w-100">
            <div class="row px-3">
                <x-form.input name="name" label="Година" size=6
                    value="{{ old('name', isset($timeline) ? $timeline->name : '') }}" />
                <x-form.input name="color" label="Цвят на фона" size=6
                    value="{{ old('color', isset($timeline) ? $timeline->color : '') }}" />
            </div>
            <ul class="nav nav-tabs">
                @foreach (langs() as $i => $lang)
                <li class="nav-item">
                    <a class="nav-link border-left-0 @if($i == 0) active @endif" id="form-locale-{{ $lang->locale }}"
                        data-toggle="tab" href="#form_{{ $lang->locale }}" role="tab">{{ $lang->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="forms">
                @foreach (langs() as $lang)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="form_{{ $lang->locale }}"
                    role="tabpanel">
                    <x-form.textarea name="data[{{ $lang->id }}][content]" label="Текст" class="text-editor" required>
                        {{ old('data.' . $lang->id . '.content', isset($timeline) ? $timeline->content($lang) : '') }}
                    </x-form.textarea>
                </div>
                @endforeach
            </div>

        </div>
    </x-form.form>
</x-admin>