<x-admin :page="$page">
    <x-table :columns="['Име', 'Действия']">
        @foreach($timelines as $timeline)
        <tr>
            <td>{{ $timeline->name }}</td>
            <td>
                <a href="{{ route('admin.timelines.edit', $timeline->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Редактирай</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$timelines />
</x-admin>