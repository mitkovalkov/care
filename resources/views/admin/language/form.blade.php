<x-admin :page=$page>
    <x-form.form :action="$action" :method="$method" :upload="true">
        <x-form.input name="name" label="Език" required size=6
            value="{{ old('name', isset($language) ? $language->name : '') }}" />

        <x-form.input name="locale" label="Код" required size=6
            value="{{ old('locale', isset($language) ? $language->locale : '') }}" />

        @if(isset($language) && $language->flag)
        <div class="form-group col-sm-12 pl-3">
            <img src="{{ $language->flag() }}" alt="flag" class="img-thumbnail thumb">
        </div>
        @endif
        <x-form.upload name="flag" label="Flag" size=6 />
    </x-form.form>
</x-admin>