<x-admin :page="$page">
    <x-table :columns="['Флаг' , 'Име', 'Код', 'Actions']">
        @foreach($languages as $lang)
        <tr>
            <td style="width:150px;">
                @if($lang->flag)
                <img src="{{ getFlagUrl($lang->flag) }}" alt="flag" class="img-thumbnail thumb"> 
                @endif
            </td>
            <td>{{ $lang->name }}</td>
            <td>{{ $lang->locale }}</td>
            <td>
                <a href="{{ route('admin.langs.edit', $lang->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Редактирай</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$languages />
</x-admin>