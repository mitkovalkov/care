<x-admin :page=$page>
    <x-form.form :action="$action" :method="$method" :upload="true">
        <div class="w-100">
            @isset($counter->img)
            <div class="pl-3"><img src="{{ $counter->img() }}" alt="photo"></div>    
            @endisset
            <div class="row px-3">
                <x-form.upload name="img" label="Photo" size="6" />

                <x-form.input name="color" label="Цвят на фона" size=6
                    value="{{ old('color', isset($counter) ? $counter->color : '') }}" />
            </div>
            <ul class="nav nav-tabs">
                @foreach (langs() as $i => $lang)
                <li class="nav-item">
                    <a class="nav-link border-left-0 @if($i == 0) active @endif" id="form-locale-{{ $lang->locale }}"
                        data-toggle="tab" href="#form_{{ $lang->locale }}" role="tab">{{ $lang->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="forms">
                @foreach (langs() as $lang)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="form_{{ $lang->locale }}"
                    role="tabpanel">
                    <div class="row px-3">
                        <x-form.input name="data[{{ $lang->id }}][name]" label="Заглавие" required size=6
                            value="{{ old('data.' . $lang->id . '.name', isset($counter) ? $counter->name($lang) : '') }}" />

                        <x-form.input name="data[{{ $lang->id }}][val]" label="Брояч" size=6
                            value="{{ old('data.' . $lang->id . '.val', isset($counter) ? $counter->val($lang) : '') }}" />
                    </div>
                    <x-form.textarea name="data[{{ $lang->id }}][content]" label="Текст">
                        {{ old('data.' . $lang->id . '.content', isset($counter) ? $counter->content($lang) : '') }}
                    </x-form.textarea>
                </div>
                @endforeach
            </div>

        </div>
    </x-form.form>
</x-admin>