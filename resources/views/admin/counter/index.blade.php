<x-admin :page="$page">
    <x-table :columns="['Име', 'Стойност', 'Действия']">
        @foreach($counters as $counter)
        <tr>
            <td>{{ $counter->name($lang) }}</td>
            <td>{{ $counter->val($lang) }}</td>
            <td>
                <a href="{{ route('admin.counters.edit', $counter->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Редактирай</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$counters />
</x-admin>