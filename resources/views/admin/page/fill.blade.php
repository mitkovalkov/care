<x-admin :page=$page_settings>
    <x-form.form :action="$action" :method="$method" :upload="true">

        <div class="w-100">
            <ul class="nav nav-tabs">
                @foreach (langs() as $i => $lang)
                <li class="nav-item">
                    <a class="nav-link border-left-0 @if($i == 0) active @endif" id="form-locale-{{ $lang->locale }}"
                        data-toggle="tab" href="#form_{{ $lang->locale }}" role="tab">{{ $lang->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content" id="forms">
                @foreach (langs() as $lang)
                <div class="tab-pane fade @if($loop->first) show active @endif" id="form_{{ $lang->locale }}"
                    role="tabpanel">
                    @foreach ($page->settings as $setting)
                    @php
                    $section = $page->setting($setting);
                    @endphp
                    @if($section['content'] == 'group')
                    <div class="border my-3">
                        <fieldset>
                            <legend class="ml-3">{{ $section['label'] }}</legend>
                    @endif
                    @if($section['content'] == 'endgroup')
                        </fieldset>
                    </div>
                    @endif
                    
                    
                    @if($section['content'] == 'input' && $section['type'] !== 'file')
                    <x-form.input name="data[{{ $lang->id }}][{{ $section['key'] }}]" :label="$section['label']"
                        :type="$section['type']" value="{{ (isset($data) && isset($data[$lang->id]) && key_exists($section['key'], $data[$lang->id]))
                            ? $data[$lang->id][$section['key']] : '' }}" />
                    @endif


                    @if($section['content'] == 'input' && $section['type'] == 'file')
                    <div class="card col-12 col-sm-4">
                        @if(isset($data) && isset($data[$lang->id]) && isset($data[$lang->id][$section['key']]) && is_image( $data[$lang->id][$section['key']]))
                        <img src="{{ (isset($data) && isset($data[$lang->id]) && key_exists($section['key'], $data[$lang->id]))
                        ? getPageUrl($data[$lang->id][$section['key']]) : '' }}">
                        @endif
                        @if(isset($data) && isset($data[$lang->id]) && isset($data[$lang->id][$section['key']]) )
                            <input type="hidden" name="data[{{ $lang->id }}][{{ $section['key'] }}]" value="{{ $data[$lang->id][$section['key']] }}">
                        @endif
                        <x-form.upload name="data[{{ $lang->id }}][{{ $section['key'] }}]" :label="$section['label']" />
                    </div>
                    @endif


                    @if($section['content'] == 'textarea')
                    <x-form.textarea name="data[{{ $lang->id }}][{{ $section['key'] }}]" :label="$section['label']"
                        :type="$section['type']">{{ (isset($data) && isset($data[$lang->id]) && key_exists($section['key'], $data[$lang->id]))
                        ? $data[$lang->id][$section['key']] : '' }}</x-form.textarea>
                    @endif


                    @if($section['content'] == 'html')
                    <x-form.html name="data[{{ $lang->id }}][{{ $section['key'] }}]" :label="$section['label']"
                        :type="$section['type']">{{ (isset($data) && isset($data[$lang->id]) && key_exists($section['key'], $data[$lang->id]))
                        ? $data[$lang->id][$section['key']] : '' }}</x-form.html>
                    @endif

                    @endforeach
                </div>
                @endforeach
            </div>

        </div>

    </x-form.form>
</x-admin>