<x-admin :page=$page_settings>
    <x-form.form :action="$action" :method="$method" :upload="true">
        <x-form.input name="slug" label="Slug" required size=6
            value="{{ old('slug', isset($page) ? $page->slug : '') }}" />
        <x-form.input name="name" label="Име" required size=6
            value="{{ old('name', isset($page) ? $page->name : '') }}" />

        <x-form.textarea name="settings" label="Настройки" placeholder="key=type;label" style="min-height: 150px"
        >{{ old('settings', isset($page) ? $page->settings() : '') }}</x-form.textarea>
        <x-form.group>
            <ul>
            <li><b>#group=text</b> Ако трябва да се обединяват няколко елемента в една секция</li>
            <li><b>#endgroup</b> Затваряне на групата</li>
            <li><b>key=type;label</b> по един на ред</li>
            <li><b>key</b> - уникално име, с което се асоциират данните.</li>
            <li><b>type</b> - вида на полето.
                <ul>
                    <li>всички видове <b>input-[type]</b> (пример: <b>input-text</b>; <b>input-email</b>)</li>
                    <li><b>textarea</b></li>
                    <li><b>html</b></li>
                </ul>
            </li>
            <li><b>label</b> - Какво да пише на полето.</li>
            </ul>
        </x-form.group>
    </x-form.form>
</x-admin>