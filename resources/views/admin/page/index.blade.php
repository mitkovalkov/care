<x-admin :page="$page">
    <x-table :columns="['Име', 'Slug', 'Действия']">
        @foreach($pages as $page)
        <tr>
            <td>{{ $page->name }}</td>
            <td>{{ $page->slug }}</td>
            <td>
                @can('admin')
                <a href="{{ route('admin.pages.edit', $page->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Настройки</a>
                @endcan
                <a href="{{ route('admin.pages.fill', $page->id) }}" class="btn btn-primary"><i class="la la-edit"></i> Попълни</a>
            </td>
        </tr>
        @endforeach
    </x-table>
    <x-paginate :paginate=$pages />
</x-admin>