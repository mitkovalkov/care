@props(['method' => 'post', 'action', 'upload' => false, 'submit' => 'Save', 'size' => 12, 'card' => true, 'inline' => false])
<div class="row">
    <div class="col-md-{{ $size }} bold-labels">
    <form method="post" action="{{ $action }}" @if($upload) enctype="multipart/form-data" class="{{ ($inline) ? 'form-inline' : '' }}"
            @endif>
            @csrf
            @if($method != 'post')
            @method($method)
            @endif
            @if($card)
            <div class="card px-2">
                <div class="card-body row">
                    {{ $slot }}
                </div>
            </div>
            @else
            {{ $slot }}
            @endif
            <x-form.submit :action="$submit" />
        </form>
    </div>
</div>
@section('before_styles')
<link rel="stylesheet" href="{{ asset('/packages/backpack/crud/css/crud.css') }}">
<link rel="stylesheet" href="{{ asset('/packages/backpack/crud/css/form.css') }}">
<link rel="stylesheet" href="{{ asset('/packages/backpack/crud/css/create.css') }}">
@endsection