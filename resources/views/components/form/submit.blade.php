@props(['action' => 'Save'])
<button type="submit" class="btn btn-primary">{{ $action}}</button>