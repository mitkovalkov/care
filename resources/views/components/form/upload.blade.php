@props(['size' => 12, 'placeholder' => '', 'name', 'label' => '', 'required'=> false, 'mb'=> 0])
<div class="form-group col-md-{{ $size }} mb-{{ $mb }} @if($required)required @endif">
    <label for="{{ $name }}">{{ $label }}</label>
    <div class="custom-file mb-4">
        <input type="file" id="{{ $name }}" name="{{ $name }}"
        {{ $attributes->merge(['class' => 'custom-file-input']) }} 
        @if($required) required @endif>
        <label class="custom-file-label" for="{{ $name }}">Choose file</label>
    </div>
</div>