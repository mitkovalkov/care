@props(['size' => 12, 'name', 'label' => '', 'required'=> false, 'checked' => false, 'emptyLabel'=> false])
<div class="form-group col-md-{{ $size }} @if($required)required @endif">
    @if($emptyLabel)
    <label>&nbsp;</label>
    @endif
    <div class="custom-control custom-switch">
        <input type="hidden" name="{{ $name }}" value="">
        <input type="checkbox" name="{{ $name }}" id="{{ $name }}"
            {{ $attributes->merge(['class' => 'custom-control-input']) }} 
            @if($required) required @endif 
            @if($checked) checked @endif>
        <label class="custom-control-label" for="{{ $name }}">{{ $label }}</label>
    </div>
    @error($name)
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>