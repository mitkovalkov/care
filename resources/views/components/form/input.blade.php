@props(['size' => 12, 'type'=>'text', 'placeholder' => '', 'name', 'label' => '', 'required'=> false])
<div class="form-group col-md-{{ $size }} @if($required)required @endif">
    <label for="{{ $name }}">{{ $label }}</label>
    <input type="{{ $type }}" name="{{ $name }}" id="{{ $name }}"
    {{ $attributes->merge(['class' => 'form-control']) }} 
    @if($required) required @endif
    placeholder="{{ $placeholder }}"
    >
    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>