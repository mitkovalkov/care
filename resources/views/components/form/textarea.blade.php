@props(['size' => 12, 'placeholder' => '', 'name', 'label' => '', 'required'=> false])
<div class="form-group col-md-{{ $size }} @if($required)required @endif">
    <label for="{{ $name }}">{{ $label }}</label>
    <textarea name="{{ $name }}" id="{{ $name }}"
        {{ $attributes->merge(['class' => 'form-control']) }} 
        @if($required) required @endif
        placeholder="{{ $placeholder }}"
    >{{ $slot }}</textarea>
    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>