@props(['size' => 12, 'name', 'label' => '', 'required'=> false, 'multiple'=> false,  ])
<div class="form-group col-sm-{{ $size }} @if($required)required @endif">
    <label for="{{ $name }}">{{ $label }}</label>
    {{ $before_select ?? ''}}
    <select name="{{ $multiple ? "{$name}[]" : $name }}" id="{{ $name }}" style="width:100%;"
    {{ $attributes->merge(['class' => 'form-control select2_field']) }} 
    @if($required) required @endif
    @if($multiple) multiple @endif
    >
    {{ $slot }}
    </select>
    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>