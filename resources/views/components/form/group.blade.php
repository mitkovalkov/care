@props(['size' => 12, 'required'=> false])
<div class="form-group col-md-{{ $size }} @if($required)required @endif">
    {{ $slot }}
</div>