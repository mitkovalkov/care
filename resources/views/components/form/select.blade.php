@props(['size' => 12, 'name', 'label' => '', 'required'=> false, ])
<div class="form-group col-md-{{ $size }} @if($required)required @endif">
    <label for="{{ $name }}">{{ $label }}</label>
    <select name="{{ $name }}" id="{{ $name }}"
    {{ $attributes->merge(['class' => 'form-control']) }} 
    @if($required) required @endif
    >
    {{ $slot }}
    </select>
    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror
</div>