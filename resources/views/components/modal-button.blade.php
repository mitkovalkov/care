@props(['target', 'color' => 'btn-light'])
<button type="button" data-toggle="modal" data-target="#{{ $target }}"
    {{ $attributes->merge(['class' => "btn $color"]) }}
>{{ $slot }}</button>