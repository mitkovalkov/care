@props(['columns' => false])
<div class="table-responsive">
    <table {{ $attributes->merge(['class' => 'table bg-white']) }} >
        @if($columns)
        <tr>
            @foreach($columns as $column)
            <th>{{ $column }}</th>
            @endforeach
        </tr>
        @endif
        {{ $slot }}
    </table>
</div>