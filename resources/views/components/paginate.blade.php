@props(['paginate' => false])
<div class="d-flex justify-content-center">
    @if($paginate)
    {!! $paginate->appends(request()->except('page'))->links() !!}
    @endif
</div>