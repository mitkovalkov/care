<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SUIS CMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="{{ route('admin.home.index') }}"><img src="{{ asset('images/logo-main.svg') }}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <!-- <li class="nav-item dropdown notification">
                            <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> <span class="indicator"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                                <li>
                                    <div class="notification-title"> Notification</div>
                                    <div class="notification-list">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item list-group-item-action active">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="../assets/images/avatar-2.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jeremy Rakestraw</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="../assets/images/avatar-3.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">
    John Abraham</span>is now following you
                                                        <div class="notification-date">2 days ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="../assets/images/avatar-4.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Monaan Pechi</span> is watching your main repository
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="../assets/images/avatar-5.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jessica Caruso</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="list-footer"> <a href="#">View all notifications</a></div>
                                </li>
                            </ul>
                        </li> -->
                        <!-- <li class="nav-item dropdown connection">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-fw fa-th"></i> </a>
                            <ul class="dropdown-menu dropdown-menu-right connection-dropdown">
                                <li class="connection-list">
                                    <div class="row">
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/github.png" alt="" > <span>Github</span></a>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/dribbble.png" alt="" > <span>Dribbble</span></a>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/dropbox.png" alt="" > <span>Dropbox</span></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/bitbucket.png" alt=""> <span>Bitbucket</span></a>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/mail_chimp.png" alt="" ><span>Mail chimp</span></a>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                            <a href="#" class="connection-item"><img src="../assets/images/slack.png" alt="" > <span>Slack</span></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="conntection-footer"><a href="#">More</a></div>
                                </li>
                            </ul>
                        </li> -->
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <!-- <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name">
                                        John Abraham</h5>
                                    <span class="status"></span><span class="ml-2">Available</span>
                                </div>
                                <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a> -->
                                <form action="{{ route('logout') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button class="dropdown-item"><i class="fas fa-power-off mr-2"></i>Logout</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Меню
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.home.index') ? 'active' : '' }}" href="{{ route('admin.home.index') }}"><i class="fa fa-fw fa-home"></i>Начало</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.slides.*') ? 'active' : '' }}" href="{{ route('admin.slides.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-slides-sub" aria-controls="menu-slides-sub"><i class="fa fa-fw fa-images"></i>Слайдове</a>
                                <div id="menu-slides-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.slides.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.slides.create') ? 'active' : '' }}" href="{{ route('admin.slides.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.slides.index') ? 'active' : '' }}" href="{{ route('admin.slides.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.news.*') ? 'active' : '' }}" href="{{ route('admin.news.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-news-sub" aria-controls="menu-news-sub"><i class="fa fa-fw fa-newspaper"></i>Новини</a>
                                <div id="menu-news-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.news.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.news.create') ? 'active' : '' }}" href="{{ route('admin.news.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.news.index') ? 'active' : '' }}" href="{{ route('admin.news.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.sectors.*') ? 'active' : '' }}" href="{{ route('admin.sectors.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-sectors-sub" aria-controls="menu-sectors-sub"><i class="fa fa-fw fa-chart-pie"></i>Сектори</a>
                                <div id="menu-sectors-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.sectors.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.sectors.create') ? 'active' : '' }}" href="{{ route('admin.sectors.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.sectors.index') ? 'active' : '' }}" href="{{ route('admin.sectors.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.standards.*') ? 'active' : '' }}" href="{{ route('admin.standards.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-standards-sub" aria-controls="menu-standards-sub"><i class="fa fa-fw fa-passport"></i>Стандарти</a>
                                <div id="menu-standards-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.standards.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.standards.create') ? 'active' : '' }}" href="{{ route('admin.standards.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.standards.index') ? 'active' : '' }}" href="{{ route('admin.standards.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.services.*') ? 'active' : '' }}" href="{{ route('admin.services.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-services-sub" aria-controls="menu-services-sub"><i class="fa fa-fw fa-briefcase"></i>Услуги</a>
                                <div id="menu-services-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.services.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.services.create') ? 'active' : '' }}" href="{{ route('admin.services.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.services.index') ? 'active' : '' }}" href="{{ route('admin.services.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.partners.*') ? 'active' : '' }}" href="{{ route('admin.partners.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-partners-sub" aria-controls="menu-partners-sub"><i class="fa fa-fw fa-handshake"></i>Партньори</a>
                                <div id="menu-partners-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.partners.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.partners.create') ? 'active' : '' }}" href="{{ route('admin.partners.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.partners.index') ? 'active' : '' }}" href="{{ route('admin.partners.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.testimonials.*') ? 'active' : '' }}" href="{{ route('admin.testimonials.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-testimonials-sub" aria-controls="menu-testimonials-sub"><i class="fa fa-fw fa-user-check"></i>Препоръки</a>
                                <div id="menu-testimonials-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.testimonials.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.testimonials.create') ? 'active' : '' }}" href="{{ route('admin.testimonials.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.testimonials.index') ? 'active' : '' }}" href="{{ route('admin.testimonials.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.team.*') ? 'active' : '' }}" href="{{ route('admin.team.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-team-sub" aria-controls="menu-team-sub"><i class="fa fa-fw fa-users"></i>Екип</a>
                                <div id="menu-team-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.team.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.team.create') ? 'active' : '' }}" href="{{ route('admin.team.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.team.index') ? 'active' : '' }}" href="{{ route('admin.team.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.users.*') ? 'active' : '' }}" href="{{ route('admin.users.index') }}" data-toggle="collapse" aria-expanded="false" data-target="#menu-users-sub" aria-controls="menu-users-sub"><i class="fa fa-fw fa-users-cog"></i>Потребители</a>
                                <div id="menu-users-sub" class="collapse submenu {{ \Illuminate\Support\Facades\Route::is('admin.users.*') ? 'show' : '' }}" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.users.create') ? 'active' : '' }}" href="{{ route('admin.users.create') }}">Създай</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{ \Illuminate\Support\Facades\Route::is('admin.users.index') ? 'active' : '' }}" href="{{ route('admin.users.index') }}">Списък</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> --}}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                @if(session()->has('alert'))
                    <div class="alert alert-{{ session()->get('alert.class') }}" role="alert">
                        <p>{{ session()->get('alert.message', '...') }}</p>
                    </div>
                @endif
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            Developed by <a href="http://cehub.bg/" target="_blank">CEHUB</a>
                        </div>
                        <!-- <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <script src="{{ asset('js/admin.js') }}"></script>
    @stack('scripts')
</body>
</html>
