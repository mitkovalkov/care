<footer class="footer section" data-aos="fade-zoom-in" data-aos-delay="300">
    <div class="container">
        <div class="flex-row">
            <div class="logo"><a href="{{ r('index') }}"><img src="{{ asset('images/logo.svg') }}" alt="Care by SAT Health"></a></div>
            <div class="footer-navigation">
                <div class="nav-box">
                    <div class="nav-title nav-link"><a href="{{ r('company')}}">{{ trans('navigation.company') }}</a></div>
                    <ul>
                        <li><span data-href="section1">{{ trans('navigation.profile') }}</span></li>
                        <li><span data-href="section2">{{ trans('navigation.pharmacovigilance') }}</span></li>
                        <li><span data-href="section3">{{ trans('navigation.partners') }}</span></li>
                        <li><span data-href="section4">{{ trans('navigation.team') }}</span></li>
                        <li><a href="{{ r('jobs.index') }}">{{ trans('navigation.careers') }}</a></li>
                        <li><a href="{{ trans('common.pricing_url') }}" target="_blank">{{ trans('common.pricing_text') }}</a></li>
                    </ul>
                </div>
                <div class="nav-box">
                    <div class="nav-title nav-link"><a href="{{ r('inner-page') }}">{{ trans('navigation.patient_support_program') }}</a></div>
                    <ul>
                        <li><span data-href="section1">{{ trans('navigation.how_we_support') }}</span></li>
                        <li><span data-href="section2">{{ trans('navigation.our_approach') }}</span></li>
                        <li><span data-href="section3">{{ trans('navigation.services') }}</span></li>
                    </ul>
                    <div class="nav-title nav-link"><a href="{{ r('homecare') }}">{{ trans('navigation.home_care') }}</a></div>
                    <ul>
                        <li><a href="{{ r('cardiology') }}">{{ trans('navigation.cardiology') }}</a></li>
                    </ul>
                </div>
                <div class="nav-box">
                    <div class="nav-title nav-link"><a href="{{ r('homecare') }}">{{ trans('navigation.information') }}</a></div>
                    <ul>
                        <li><a href="{{ url(\App::getLocale() .'/terms-and-conditions') }}">{{ trans('navigation.terms_and_conditions') }}</a></li>
                        <li><a href="{{ url(\App::getLocale() .'/privacy-policy') }}">{{ trans('navigation.privacy_policy') }}</a></li>
                        <li><a href="{{ url(\App::getLocale() .'/uci-policy') }}">{{ trans('navigation.uci_policy') }}</a></li>
                        <li><a href="{{ url(\App::getLocale() .'/privacy-notice') }}">{{ trans('navigation.privacy_notice') }}</a></li>
                    </ul>
                </div>
                <div class="nav-box">
                    <div class="nav-title nav-link"><a href="{{ r('contact') }}">{!! trans('contacts.location') !!}</a></div>
                    <div class="text">
                        <p>{!! trans('contacts.address') !!}</p>
                        <a href="mailto:{{ trans('contacts.email') }}">{{ trans('contacts.email') }}</a>
                        <a href="tel:{{ trans('contacts.phone_number') }}">{{ trans('contacts.phone_text') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="flex-row">
                <div class="flex-box">
                    <p>{{ trans('common.copyright') }} © {{ date('Y') }} SatHealth.com</p>
                    <ul>
                        {{--<li><a href="">{{ trans('navigation.therms_and_condition') }}</a></li>--}}
                    </ul>
                </div>
                <div class="flex-box width-auto">
                    <div class="social-nav">
                        <ul>
                            <li><a href="https://www.linkedin.com/company/sat-health/" target="_blank"><img src="{{ asset('images/linkedin-icon.svg') }}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
