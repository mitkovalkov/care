<header class="header {{ Route::is('index') ? 'homepage-header' : '' }}" id="header">
    <div class="container container-big">
        <div class="logo"><a href="{{ r('index') }}"><img src="{{ asset('images/logo.png') }}" alt="Care by SAT Health"></a></div>
        <div class="main-navigation">
            <ul>
                <li class="nav-box nav-covid"><a href="{{ r('covid-19') }}">{{ trans('navigation.covid_19') }}</a></li>
                <li class="nav-box"><span class="nav-link"><a href="{{ r('company') }}">{{ trans('navigation.about') }}</a></span>
                    <ul>
                        <li><span data-href="section1">{{ trans('navigation.profile') }}</span></li>
                        <li><span data-href="section2">{{ trans('navigation.pharmacovigilance') }}</span></li>
                        <li><span data-href="section3">{{ trans('navigation.partners') }}</span></li>
                        <li><span data-href="section4">{{ trans('navigation.team') }}</span></li>
                        <li><a href="{{ r('jobs.index') }}">{{ trans('navigation.careers') }}</a></li>
                    </ul>
                </li>
                <li class="nav-box"><span class="nav-link"><a href="{{ r('inner-page') }}">{{ trans('navigation.patient_support_program') }}</a></span>
                    <ul>
                        <li><span data-href="section1">{{ trans('navigation.how_we_support') }}</span></li>
                        <li><span data-href="section2">{{ trans('navigation.our_approach') }}</span></li>
                        <li><span data-href="section3">{{ trans('navigation.services') }}</span></li>
                    </ul>
                </li>
                <li class="nav-box"><span class="nav-link"><a href="{{ r('homecare') }}">{{ trans('navigation.home_care') }}</a></span>
                    <ul>
                        <li><a href="{{ r('cardiology') }}">{{ trans('navigation.cardiology') }}</a></li>
                    </ul>
                </li>
                <li class="nav-box"><span class="nav-link"><a href="{{ r('contact') }}">{{ trans('navigation.contact') }}</a></span></li>
            </ul>
        </div>
        <ul class="lng-switcher">
        @if(app()->getLocale() == 'en')
            <li><a href="{{ changeLang('bg') }}"><div class="icon icon-bg-flag"></div>BG</a></li>
        @else
            <li><a href="{{ changeLang('en') }}"><div class="icon icon-en-flag"></div>EN</a></li>
        @endif
        </ul>
        <div id="toggle-nav"></div>
    </div>
</header>