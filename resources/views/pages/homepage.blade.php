@extends('layouts.main')

@section('content')
    <div class="section section-site-nav">
        <div class="container justiy-content-center">
            <h3 class="animated-item-bottom"><a href="{{ r('company') }}">{{ trans('navigation.about_headeing') }} <div class="icon"><img src="{{ asset('/images/about.png') }}" alt="Care About"></div></a></h3>
            <h3 class="animated-item-bottom"><a href="{{ r('homecare') }}">{{ trans('navigation.home_care_headeing') }} <div class="icon"><img src="{{ asset('/images/home.png') }}" alt="Home Care"></div></a></h3>
            <h3 class="animated-item-bottom"><a href="{{ r('inner-page') }}">{{ trans('navigation.patient_support_program_headeing') }} <div class="icon"><img src="{{ asset('/images/family.png') }}" alt="Care Family"></div></a></h3>
        </div>
    </div>
    @include('sections.video')
    @include('sections.our-mission')
    @include('sections.about-us', [
        'image1' => asset('images/about01.jpg'),
        'image2' => asset('images/about02.jpg'),
        'image3' => asset('images/about03.jpg'),
        'image4' => asset('images/about04.jpg'),
        'title' => $data['company_title'],
        'text' => $data['company_text'],
        'btn_url' => $data['company_url_button'],
        'btn_text' => $data['company_text_button']
    ])
    @include('sections.counter')
    @include('sections.programs', ['title' => $data['psp_title'], 'text' => $data['psp_text'], 'button' => true, 'image' => asset('images/hwsp.jpg'), 'section_gray' => false])
    @include('sections.testimonials')
@endsection