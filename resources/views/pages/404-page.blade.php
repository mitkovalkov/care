@include('sections.page-header', [
    'title' => trans('common.page_error_title'),
    'btn' => array(
        'name' => trans('navigation.home'),
        'link' => '/',
    ),
]);
