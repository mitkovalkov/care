@extends('layouts.main')

@section('content')
    @include('sections.page-header', [
        'title' => $data['covid_title'],
        'image' => getPageUrl($data['covid_header_image']),
        'btn' => array(
            'name' => trans('common.contact_us') . ' ' . trans('contacts.phone_text'),
            'class' => 'btn-primary has-icon-phone',
            'link' => 'tel:' . trans('contacts.phone_number'),
        ),
    ])

    @include('sections.covid', [
        'text' => $data['covid_text'],
        'image' => getPageUrl($data['covid_image'])
    ])

    @include('sections.partners', [
        'title' => $data['partern_title'],
        'text' => $data['partern_text'],
        'image' => getPageUrl($data['partern_image'])
    ])

    <div class="section section-jobs section-price-table padding-lg section-gray-bg">
        <div class="container">
            <div class="accordion animated-item-bottom">
                <div class="accordion-title justify-content-center">{{ $data['prices_title'] }} <div class="icon icon-next"></div></div>
                <div class="accordion-content">
                    <div class="text">
                        {!! $data['prices_text'] !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
