@extends('layouts.main')

@section('content')
    @include('sections.page-header', ['title' => $data['psp_header_title'], 'image' => getPageUrl($data['psp_header_image'])])
    @include('sections.text-image')
    @include('sections.carousel-text')
    @include('sections.services')
    @include('sections.testimonials')
@endsection