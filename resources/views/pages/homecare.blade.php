@extends('layouts.main')

@section('content')
    @include('sections.page-header', [
        'title' => trans('navigation.home_care'),
        'image' => getPageUrl($data['homecare_about_image'])
    ])

    @include('sections.company-info', [
        'title' => $data['homecare_about_title'],
        'text' => $data['homecare_about_text'],
        'video' => false
    ])

    @include('sections.active-programs', [
        'title' => $data['active_programs_title'],    
        'text' => $data['active_programs_text'],
        'active_programs' => [
            'active_program_1' => [
                'title' => $data['active_program_1_title'],
                'url' => $data['active_program_1_link'],
                'image' => $data['active_program_1_image'],
                'text' => $data['active_program_1_text']
            ],
            'active_program_2' => [
                'title' => $data['active_program_2_title'],
                'url' => $data['active_program_2_link'],
                'image' => $data['active_program_2_image'],
                'text' => $data['active_program_2_text']
            ],
            'active_program_3' => [
                'title' => $data['active_program_3_title'],
                'url' => $data['active_program_3_link'],
                'image' => $data['active_program_3_image'],
                'text' => $data['active_program_3_text']
            ]
        ]
    ])

    @include('sections.title', [
        'title' => $data['homecare_providing_title']
    ])

    <div class="section-left-right">
        @include('sections.about-us', [
            'image1' => getPageUrl($data['homecare_providing_images_1']),
            'image2' => getPageUrl($data['homecare_providing_images_2']),
            'image3' => getPageUrl($data['homecare_providing_images_3']),
            'image4' => getPageUrl($data['homecare_providing_images_4']),
            'title' => $data['homecare_providing_title_1'],
            'text' => $data['homecare_providing_text_1']
        ])
        
        @include('sections.programs', [
            'title' => $data['homecare_providing_title_2'],
            'text' => $data['homecare_providing_text_2'],
            'button' => false,
            'image' => getPageUrl($data['homecare_providing_image_2']),
            'section_gray' => false
        ])

        @include('sections.programs', [
            'title' => $data['homecare_providing_title_3'],
            'text' => $data['homecare_providing_text_3'],
            'button' => false,
            'image' => getPageUrl($data['homecare_providing_image_3']),
            'section_gray' => false
        ])
    </div>
    @include('sections.icons', [
        'title' => $data['homecare_providing_title_advantages'],
        'homecare_advantages_title_1' => $data['homecare_advantages_title_1'],
        'homecare_advantages_image_1' => getPageUrl($data['homecare_advantages_image_1']),
        'homecare_advantages_title_2' => $data['homecare_advantages_title_2'],
        'homecare_advantages_image_2' => getPageUrl($data['homecare_advantages_image_2']),
        'homecare_advantages_title_3' => $data['homecare_advantages_title_3'],
        'homecare_advantages_image_3' => getPageUrl($data['homecare_advantages_image_3']),
        'homecare_advantages_title_4' => $data['homecare_advantages_title_4'],
        'homecare_advantages_image_4' => getPageUrl($data['homecare_advantages_image_4']),
        'homecare_advantages_title_5' => $data['homecare_advantages_title_5'],
        'homecare_advantages_image_5' => getPageUrl($data['homecare_advantages_image_5']),
        'homecare_advantages_title_6' => $data['homecare_advantages_title_6'],
        'homecare_advantages_image_6' => getPageUrl($data['homecare_advantages_image_6']),
        'homecare_advantages_title_7' => $data['homecare_advantages_title_7'],
        'homecare_advantages_image_7' => getPageUrl($data['homecare_advantages_image_7']),
        'homecare_advantages_title_8' => $data['homecare_advantages_title_8'],
        'homecare_advantages_image_8' => getPageUrl($data['homecare_advantages_image_8']),
        'section_bg' => 'section-gray-bg'
    ])
@endsection