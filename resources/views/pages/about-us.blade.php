@extends('layouts.main')

@section('content')
    @include('sections.page-header', ['title' => trans('common.about_us'), 'image' => getPageUrl($data['about_company_image'])])
    @include('sections.company-info', ['title' => $data['about_company_title'], 'text' => $data['about_company_text'], 'video' => true])
    @include('sections.timeline')
    @include('sections.pharmacovigilance', ['title' => $data['pharmacovigilance_title'], 'text' => $data['pharmacovigilance_text'], 'image' => asset('images/pharma-img.jpg'), 'button_text' => $data['pharmacovigilance_text_button'], 'modal_text' => $data['pharmacovigilance_modal']])
    @include('sections.clients')
    @include('sections.our-team', ['title' => $data['team_company_title'], 'text' => $data['team_company_text'], 'team_title' => $data['team_company_title_team']])
@endsection