@extends('layouts.main')

@section('content')
<div class="section">
    <div class="container">
        <div class="text animated-item-bottom">
            {!! $data['simple_text'] !!}
        </div>
    </div>
</div>
@endsection