@extends('layouts.main')

@section('content')
    @include('sections.page-header', ['title' => trans('contacts.contact_us'), 'image'])
    @include('sections.contacts-info')
    @include('sections.gmap')
@endsection